from pymongo import MongoClient

class BipolarRepository(object):
    """ Repository implementing CRUD operations on tweet collection in MongoDB """
    def __init__(self):
        self.client = MongoClient(host='localhost', port=27017)
        self.database = self.client['test']
        self.collection = self.database.bipolarTweet

    def create(self, object):
        if object is not None:
            self.collection.insert(object.get_as_json())
        else:
            raise Exception("Tidak ada yang disimpan, paramaternya masih None")

    def read(self, id=None):
        if id is None:
            return self.collection.find({})
        else:
            return self.collection.find({"_id":id})

    def update(self, object):
        if object is not None:
            self.collection.save(object.get_as_json())
        else:
            raise Exception("tidak bisa update, parameter objectnya tidak ada")

    def delete(self, object):
        if object is not  None:
            self.collection.remove(object.get_as_json())
        else:
            raise Exception("tidak bisa delete, tidak ada param")