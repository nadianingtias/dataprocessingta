import glob
import csv
import cld2
from DBRepository.UserAllRepository import UserAllRepository as UserAllRepo
from Model.User import User

def readFolderCSV(dir):
    csvDir = glob.glob(dir + '/*.csv');
    filenamecsv = []
    i = 0;
    for file in csvDir:
        i=i+1
        nameOfFile = file
        space = nameOfFile.find(" ")
        file = nameOfFile[54:]
        num = nameOfFile[54:space]
        nameCSV = nameOfFile[space+1:]
        name = nameCSV[:-11]
        # print("{} | {} || : {} | {} | {}" .format(i, file, num, nameCSV, name))
        filenamecsv.append(file)
        # singleCSV = csv.reader(open(file, 'r', errors='ignore'), delimiter=',')
        # for row in singleCSV:
        #     print(i)
        #     i = i + 1
            # newTweet = Tweet(None, row[1], row[2], row[5], row[9],state)
            # insertTweet(repoTweet, newTweet)
            # print("ok")
    print(i)
    # 2214 records
    return filenamecsv

if __name__ == '__main__':
    dir = "C:\\Users\\Nadian\\PycharmProjects\\mypython\\TweetPerUser"
    # read file-file csv di dalam 1 folder untuk bisa diakses
    fileNameCSVs = []
    fileNameCSVs = readFolderCSV(dir)
    counterFile = 0
    for file in fileNameCSVs:
        counterFile=counterFile+1

        print("file {} : {}".format(counterFile, file))
        filedir = dir+"\\"+file
        f = open(filedir, 'r')
        with f:
            mergetweet = ""
            baris = 0
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                i=0
                baris = baris+1
                for e in row:
                    if(i==2):
                        mergetweet = mergetweet+ e
                    i = i+1
            jumlahRowTweets = (baris-2)/2
            print("Total Tweets user {} : {} tweets".format(file,jumlahRowTweets))
            if(jumlahRowTweets>25):
                isActiveUser = True
            else:
                isActiveUser = False
        print("---------------------------------")
        isReliable, textBytesFound, details = cld2.detect(mergetweet)
        lang = details[0][1]
        print('  reliable: %s' % isReliable)
        print('  textBytes: %s' % textBytesFound)
        print('  details: %s' % str(details))
        print("---------------------------------")
        if (isActiveUser):
            print("OK this user is active")
        if ((isReliable == True) & (lang == "en")):
            print("OK english")

        nameOfFile = file
        space = nameOfFile.find(" ")
        num = nameOfFile[0:space]
        uName = nameOfFile[space+1:-11]
        print(uName)

        repoUser = UserAllRepo()
        findUser = repoUser.searchName(uName)
        objUserFind = User()
        objUserALLUpdate = User()
        isSuccess = False
        for tweet in findUser:
            isSuccess = True
            tem_tweet = objUserFind.build_from_json(tweet)
            objUserALLUpdate = User(tweet["_id"], tweet["username"], tweet["date"], tweet["text"], tweet["tweet_id"],
                                    tweet["state"], isActiveUser, lang)
            repoUser.update(objUserALLUpdate)
            print("sukses")
        if not isSuccess:
            print("User tidak ditemukan di DB")
        print("===== ===== ===== END USER ===== ===== =====")
