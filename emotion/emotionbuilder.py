import glob
import pandas as pd

from textblob import TextBlob
from pyFeel import Feel

from Model.LangStyle import Emotion
from DBRepository.UserTweetRepository import UserTweetRepository as UserTweetRepo
from Model.User import User
from Model.LangStyle import UserTweet as Utweet

def readFolderCSV(dir):
    csvDir = glob.glob(dir + '/*.csv');
    filenamecsv = []
    i = 0;
    for file in csvDir:
        i = i + 1
        nameOfFile = file
        space = nameOfFile.find(" ")
        file = nameOfFile[54:]
        file = nameOfFile[61:]
        num = nameOfFile[54:space]
        nameCSV = nameOfFile[space + 1:]
        name = nameCSV[:-11]
        # print("{} | {} || : {} | {} | {}" .format(i, file, num, nameCSV, name))
        filenamecsv.append(nameOfFile)
    print("jumlah CSV : {}".format(i))
    # 2214 records
    return filenamecsv

def findUname(file):
    nameOfFile = file
    space = nameOfFile.find(" ")
    num = nameOfFile[0:space]
    username = nameOfFile[space + 1:-11]
    return username

def getObjUserDepressionTweet(username):
    repoUserTweet = UserTweetRepo()
    findUser = repoUserTweet.searchName(username)
    # print("usernya :->{}<-".format(findUser.__dict__))
    idUser = None
    objUserTweetFind = Utweet()
    isSuccess = False
    for successresult in findUser:
        # print("user : {}".format(successresult))
        isSuccess = True
        objUserTweetFind = Utweet()
        # print("sumbernya : {}".format(type(successresult)))
        print("after built 1st : {}".format(type(objUserTweetFind)))
        objUserTweetFind = Utweet(successresult['username'], successresult['tweets'])
        # idUser = objUserTweetFind.
        idUser = successresult['_id']
        # tweets = successresult['tweets']
        print("id was found : {}".format(idUser))
        # for i in range(len(tweets)):
            #
            # print(successresult['tweets'])
            # objUserTweetFind.

        # print(objUserTweetFind.__dict__)
    if not isSuccess:
        print("User tidak ditemukan di DB")
    if objUserTweetFind:
        # print("before return : {}".format(type(objUserTweetFind)))
        return objUserTweetFind
    else:
        return None


def getEmotionObj(rowText):
    # GET EMOTION WITH pyFeel
    # 1. translate french with textblob
    rowText = TextBlob(rowText)
    translated = rowText.translate(to='fr')
    # 2. get emotion
    emotionResult = Feel(str(translated)).emotions()
    # print("1. emotion result : {}".format(emotionResult))
    # 3. return emotion
    emotionIns = Emotion(None, emotionResult['positivity'], emotionResult['joy'], emotionResult['fear'],
                         emotionResult['sadness'], emotionResult['angry'], emotionResult['surprise'],
                         emotionResult['disgust'])
    return emotionIns


def getEmotion(usertweet):
    # df = pd.read_csv(file, encoding='latin-1')
    # print(df.shape[0])
    # arrayEmotionIns = []
    arrayTweet = usertweet.tweets
    for i in range(len(arrayTweet)):
        print(arrayTweet[i])
    pass

def getArrayEmotion(file):
    df = pd.read_csv(file, encoding='latin-1')
    print(df.shape[0])
    arrayEmotionIns = []
    for count in range(df.shape[0]):
        print("count : {}".format(count))
        rowText = str(df['text'][count])
        timeRowText = df['created_at'][count]
        # 1. koreksi
        blo = TextBlob(rowText)
        corrected = blo.correct()
        rowText = str(corrected)
        objEmotion = getEmotionObj(rowText)
        arrayEmotionIns.append(objEmotion)
        print("result emotion : {}".format(objEmotion.__dict__))

    print("total array emo: {}".format(len(arrayEmotionIns)))
    return arrayEmotionIns

if __name__ == '__main__':
    import time

    start_time = time.time()

    dir = "C:\\Users\\Nadian\\PycharmProjects\\resultdone1"
    # read file-file csv di dalam 1 folder untuk bisa diakses
    fileNameCSVs = []
    fileNameCSVs = readFolderCSV(dir)
    count=0
    for file in fileNameCSVs:
        count = count +1
        print(count)
        print(file)
        username = findUname(file)
        print("username : {}".format(username))
        objUserTweetInsPrev = Utweet()
        # print("before method: {}".format(type(objUserTweetInsPrev)))
        objUserTweetInsPrev = getObjUserDepressionTweet(username)
        # print("after method : {}".format(type(objUserTweetInsPrev)))
        arrayTweet = objUserTweetInsPrev.tweets
        # for i in range(len(arrayTweet)):
        for tweet in arrayTweet:
            print(tweet)
            # print(arrayTweet[i])
        pass
        arrayEmotion = []
        # getEmotion(objUserTweetInsPrev)
        arrayEmotion = getArrayEmotion(file, )

        # for array in arrayEmotion:
        #     print(array)