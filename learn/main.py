from learn.ProjectRepository import ProjectRepository
from learn.MyProject import MyProject
from learn.Tweet import Tweet
from learn.TweetRepository import TweetRepository
import os
import pandas
import glob
imdir="C:\\Users\\Nadian\\PycharmProjects\\mypython\\learn\\csv\\BIPOLAR"
import csv

def load_all_items_from_database(repository):
    print("Loading all items from database : ")
    projects = repository.read()
    at_least_one_item = False
    for p in projects:
        at_least_one_item = True
        tmp_project = MyProject.build_from_json(p)
        print("ID = {} | Username = {} | Date = {} | text = {} | text_id = {} ".format(tmp_project._id, tmp_project.username, tmp_project.date, tmp_project.text, tmp_project.text_id))
    if not  at_least_one_item:
        print("no item in database")

def test_create(repository, new_project):
    print("\n\n saving new_project to DB")
    repository.create(new_project)
    print("new project saved into DB")
    print("Loading new_project from DB")
#proses membaca entry dengan param ID yang baru dimasukkan
    db_projects = repository.read(project_id = new_project._id)
    for p in db_projects:
        project_from_db = MyProject.build_from_json(p)
        print("new_project have been inserted = {}".format(project_from_db.get_as_json()))

def test_update(repository, new_project):
    print("\n\n updating new_projet in DB")
    db_projects = repository.read(project_id=new_project._id)
    if(db_projects):
        for p in db_projects:
            project_from_db = MyProject.build_from_json(p)
            print("entri lama di DB = {}".format(project_from_db.get_as_json()))

        repository.update(new_project)
        print("success to update")
        # proses baca data yang sudah diupdate
        db_projects_updated = repository.read(project_id=new_project._id)
        for p in db_projects_updated:
            project_from_db = MyProject.build_from_json(p)
            print("entri baru di DB = {}".format(project_from_db.get_as_json()))

    else:
        print("belum ada entri itu, lakukan entry data ?")
        answer = input()
        if(answer == 'y'):
            test_create(new_project)
        else:
            print("oke saya tidak akan memasukkannya")

def test_delete(repository, new_project):
    print("\n\nDeleting new_project from database")
    repository.delete(new_project)

    print("new_project deleted from database")
    print("Trying to reload new_project from database")
    db_projects = repository.read(project_id=new_project._id)
    found = False
    for p in db_projects:
        found = True
        project_from_db = MyProject.build_from_json(p)
        print("new_project still exist = {}".format(project_from_db.get_as_json()))

    if not found:
        print("Item with id = {} was deleted,not found in the database".format(new_project._id))


def main():
    repository = ProjectRepository()

    # display all items from DB
    load_all_items_from_database(repository)

    # create new_project and read back from database
    # bikin objek dari data json
    jsondata = {}
    jsondata = {"username": "Madianmyname",
                "date": "10/11/2018",
                "text": "This should be a very simple website, based on wordpress with functionalities for Freelancers",
                "text_id": 2233}
    new_project = MyProject.build_from_json(jsondata)

    print(new_project.username)
    #memakai objek untuk create di DB
    test_create(repository, new_project)

    # update new_project and read back from database
    new_project.username = "mayamadian"
    test_update(repository, new_project)

    # delete new_project and try to read back from database
    test_delete(repository, new_project)

def insertTweet(repo, new_object):
    repo.create(new_object)
    # proses membaca entry dengan param ID yang baru dimasukkan
    db_datas = repo.read(id=new_object._id)
    for p in db_datas:
        data_from_db = Tweet.build_from_json(p)
        print("New Item have been inserted = {}".format(data_from_db.get_as_json()))

def load_all_items_from_database(repository):
    print("Loading all items from database : ")
    projects = repository.read()
    at_least_one_item = False
    for p in projects:
        at_least_one_item = True
        tmp_project = MyProject.build_from_json(p)
        print("ID = {} | Username = {} | Date = {} | text = {} | text_id = {} ".format(tmp_project._id, tmp_project.username, tmp_project.date, tmp_project.text, tmp_project.text_id))
    if not  at_least_one_item:
        print("no item in database")
    pro = repository.read("5bd6da744d7b542b60e93a7b")
    for p in pro:
        print("{}".format(p.username))

def mainmain():
    repoTweet = TweetRepository()
    jsondata = {"username": "Madianmyname",
                "date": "10/11/2018",
                "text": "This should be a very simple website, based on wordpress with functionalities for Freelancers",
                "tweet_id": 2233}
    new_tweet = Tweet.build_from_json(jsondata)
    # print(new_tweet.getText())
    insertTweet(repoTweet,new_tweet)



def concat2():
    csvfiles = glob.glob(imdir+'/*.csv')
    wf = csv.writer(open(imdir+'/outputnya.csv','w', errors='ignore'), delimiter=',')
    i=0
    # print(csvfile)
    for file in csvfiles:
        print(file)
        rd = csv.reader(open(file, 'r', errors='ignore'), delimiter=',')
        # while rd:
        #     print(rd)
        for row in rd:
            i=i+1
            # print(row)
            wf.writerow(row)
        print(i)

def mergeCSV(dir):
    # print("the direktory is ", dir)
    # csvfiles = glob.glob(imdir + '/*.csv')
    csvDir = glob.glob(dir + '/*.csv');
    writtenFile = csv.writer(open(dir+'\\mergedFile.csv', 'w', errors='ignore'), delimiter=',')
    i=0;
    for file in csvDir:
        singleCSV = csv.reader(open(file,'r', errors='ignore'),delimiter=',')
        for row in singleCSV:
            writtenFile.writerow(row)
            i=i+1
    print("has been generated %d rows data"% i)

def mergeAll():
    dir = "C:\\Users\\Nadian\\PycharmProjects\\mypython\\learn\\csv\\"
    dirs = []
    labels = ["BIPOLAR", "DEPRESSION", "PTSD", "SAD"]

    for label in labels:
        # tmp = '"'+dir+label+'"'
        tmp = dir + label
        dirs.append(tmp)
        # print(tmp)
    for d in dirs:
        print(d)
        mergeCSV(d)

    # mergeCSV(dir+labels[0])
def importTweetBIPOLAR(repoTweet,dir):
    state="BIPOLAR"
    csvDir = glob.glob(dir + '/*.csv');
    i = 0;
    for file in csvDir:
        # name = file.split(', ', 7)
        print(file)
        singleCSV = csv.reader(open(file, 'r', errors='ignore'), delimiter=',')
        for row in singleCSV:
            print(i)
            i = i + 1
            if(i%2==0):
                newTweet = Tweet(None, row[1], row[2], row[5], row[9],state)
                insertTweet(repoTweet, newTweet)
                print("ok")

def importTweetDepr(repoTweet,dir):
    state="DEPRESSION"
    csvDir = glob.glob(dir + '/*.csv');
    i = 0;
    for file in csvDir:
        # name = file.split(', ', 7)
        print(file)
        singleCSV = csv.reader(open(file, 'r', errors='ignore'), delimiter=',')
        for row in singleCSV:
            print(i)
            i = i + 1
            newTweet = Tweet(None, row[1], row[2], row[5], row[9],state)
            insertTweet(repoTweet, newTweet)
            print("ok")
def importTweetPTSD(repoTweet,dir):
    state="PTSD"
    csvDir = glob.glob(dir + '/*.csv');
    i = 0;
    for file in csvDir:
        # name = file.split(', ', 7)
        print(file)
        singleCSV = csv.reader(open(file, 'r', errors='ignore'), delimiter=',')
        for row in singleCSV:
            print(i)
            i = i + 1
            newTweet = Tweet(None, row[1], row[2], row[5], row[9],state)
            insertTweet(repoTweet, newTweet)
            print("ok")
def importTweetSAD(repoTweet,dir):
    state="SAD"
    csvDir = glob.glob(dir + '/*.csv');
    i = 0;
    for file in csvDir:
        # name = file.split(', ', 7)
        print(file)
        singleCSV = csv.reader(open(file, 'r', errors='ignore'), delimiter=',')
        for row in singleCSV:
            print(i)
            i = i + 1
            newTweet = Tweet(None, row[1], row[2], row[5], row[9],state)
            insertTweet(repoTweet, newTweet)
            print("ok")

if __name__ == '__main__':
    print("1")
    dir = "C:\\Users\\Nadian\\PycharmProjects\\mypython\\learn\\csv\\" + "DEPRESSION"
    # mergeCSV(dir)
    print("2")
    # mergeDir = "C:Users/Nadian/PycharmProjects/mypython/learn/mergedCSV/BIPOLARmergedFile.csv"
    tweetRepo = TweetRepository()
    # mergeDir = "C:\\Users\\Nadian\\PycharmProjects\\mypython\\learn\\csv\\BIPOLAR"
    # importTweetBIPOLAR(tweetRepo,mergeDir)
    # mergeDir = "C:\\Users\\Nadian\\PycharmProjects\\mypython\\learn\\csv\\DEPRESSION"
    # importTweetDepr(tweetRepo, mergeDir)
    # mergeDir = "C:\\Users\\Nadian\\PycharmProjects\\mypython\\learn\\csv\\PTSD"
    # importTweetPTSD(tweetRepo, mergeDir)
    mergeDir = "C:\\Users\\Nadian\\PycharmProjects\\mypython\\learn\\csv\\SAD"
    importTweetSAD(tweetRepo, mergeDir)
    # csvDir = glob.glob(mergeDir);
    # csvfile = open('mergeDir', errors='ignore')
    # readCSV = csv.reader(csvfile, delimiter=',')
    #
    # for row in readCSV:
    #     print(row[1])
    #     newTweet = Tweet(row[1], row[2], row[5], row[9])
    #     insertTweet(tweetRepo, newTweet)

    # main()
    # mainmain()
    # main3()
    # concate()
    # concat2()
    #menggabung perolehan csv dari keyword yang sudah dibuat
    repoproject = ProjectRepository()
    load_all_items_from_database(repoproject)

    distProjects = repoproject.distinctRawData()

    # print(list(distProjects))
    print(distProjects)
    for p in distProjects:
        print(p)
        # print(p['_id']['username'])
        # print(p['_id']['text'])

    # while distProjects.next():
    #     print(distProjects.username)

    # import json
    # x = json.loads(list(distProjects))
    # y = json.dumps(x)
    # print(x)