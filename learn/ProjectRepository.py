from pymongo import MongoClient
# from learn.myproject import MyProject
from learn.MyProject import MyProject

class ProjectRepository(object):
    """ Repository implementing CRUD operations on projects collection in MongoDB """
    def __init__(self):
        self.client = MongoClient(host='localhost', port=27017)
        self.database = self.client['test']
        self.collection = self.database.projects

    def create(self, object):
        if object is not None:
            self.collection.insert(object.get_as_json())
        else:
            raise Exception("Tidak ada yang disimpan, paramaternya masih None")

    def read(self, project_id=None):
        if project_id is None:
            return self.collection.find({})
        else:
            return self.collection.find({"_id":project_id})

    def update(self, myproject):
        if myproject is not None:
            self.collection.save(myproject.get_as_json())
        else:
            raise Exception("tidak bisa update, parameter projectnya tidak ada")

    def delete(self, myproject):
        if myproject is not  None:
            self.collection.remove(myproject.get_as_json())
        else:
            raise Exception("tidak bisa delete, tidak ada param")

    def distinctRawData(self):
        distinct = self.collection.aggregate(
                                [
                                    {"$group": {"_id": {"username": "$username", "text": "$text"}}}
                                ]
                            )
        return distinct