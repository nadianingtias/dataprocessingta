import pandas as pd
import glob
import spacy

from nltk.corpus import words
from textblob import TextBlob
nlp = spacy.load('en_core_web_sm')
from Model.LangStyle import Emotion
from Model.LangStyle import Annotation


def sentiment_scores(docx):
    return sent_analyzer.polarity_scores(docx.text)


def readFolderCSV(dir):
    csvDir = glob.glob(dir + '/*.csv');
    filenamecsv = []
    i = 0;
    for file in csvDir:
        i = i + 1
        nameOfFile = file
        space = nameOfFile.find(" ")
        file = nameOfFile[54:]
        file = nameOfFile[61:]
        num = nameOfFile[54:space]
        nameCSV = nameOfFile[space + 1:]
        name = nameCSV[:-11]
        # print("{} | {} || : {} | {} | {}" .format(i, file, num, nameCSV, name))
        filenamecsv.append(nameOfFile)
    print("jumlah CSV : {}".format(i))
    # 2214 records
    return filenamecsv


def getPOSTagTextBlob(file):
    df = pd.read_csv(file, encoding='latin-1')
    print(df.shape[0])
    username = file[47:]
    # username = file[48:]
    print("username : {}".format(username))
    arrayTweetRowIns = []  # untuk menyimpan sejumlah tweet da;am 1 CSV user
    # count = 186
    for count in range(1):
        # while count <188:
        # count=count+1
        # =================Sentence sentimen
        created_at = ''
        tokenSize = 0
        negSentimen = 0
        posSentimen = 0
        neuSentimen = 0
        uniSentimen = ''
        multiSentimen = []
        # =================POS tagging
        aux = []
        conj = []
        adv = []
        impronoun = []
        perpronoun = []
        prep = []
        func = []
        negation = []
        filler = []
        # =================kata2 positif / neatif
        posAnno = []
        negAnno = []

        sample = df['text'][count]
        docRaw = TextBlob(sample)
        doc= docRaw.correct()               #textblob method - untuk mengkoreksi pengejaan kata
        created_at = df['created_at'][count]
        tokenSize = len(doc)
        # negSentimen = doc._.sentimenter['neg']
        # posSentimen = doc._.sentimenter['pos']
        # neuSentimen = doc._.sentimenter['neu']
        neuSentimen = doc.sentiment.polarity
        if neuSentimen > 0.33:
            uniSentimen = 'positive'
            multiSentimen.append('positive')
        elif neuSentimen < -0.33:
            uniSentimen = 'negative'
            multiSentimen.append('negative')
        else:
            uniSentimen = 'neutral'
            multiSentimen.append('neutral')
            if neuSentimen > 0.1 :
                multiSentimen.append('positive')
            elif neuSentimen < -0.1 :
                multiSentimen.append('negative')
        POSTag = doc.tags
        print("POS tag : {} ".format(POSTag))
        for token, tag in doc.tags:
            print(token, tag)
            if tag == 'RB' and (token == 'not' or token == "n't"):
                negation.append(token)
            if tag =='CC':
                conj.append(token)
                func.append(token)
            if tag == 'RB' or tag == 'RBR' or tag == 'RBS':
                adv.append(token)
            if tag == 'PRP$':
                impronoun.append(token)
                func.append(token)
            if  tag == 'PRP':
                perpronoun.append(token)
                func.append(token)
            if tag == 'IN':
                prep.append(token)
                func.append(token)
            if tag == 'DT' or tag == 'RP':
                func.append(token)
            if tag == 'UH':
                filler.append(token)
        for i in range(len(doc)):
            token = doc[i]
            # print(token.tag)





        print("======================={}==========================".format(count + 1))
        print('------------------------sentence------------------------')
        print("created_at : {}".format(created_at))
        print("text : {}".format(doc))
        print("ukuran : {}".format(tokenSize))
        print("posSentiment : {}".format(posSentimen))
        print("negSentiment : {}".format(negSentimen))
        print("neuSentiment : {}".format(neuSentimen))
        print("uniSentiment : {}".format(uniSentimen))
        print("multiSentiment : {}".format(multiSentimen))
        print('-------------------------POS tagging-----------------------')
        print("{} Auxiliary : {}".format(len(aux), aux))
        print("{} Conjunction : {}".format(len(conj), conj))
        print("{} Adverb : {}".format(len(adv), adv))
        print("{} Impersonal Pronoun : {}".format(len(impronoun), impronoun))
        print("{} Personal Pronoun : {}".format(len(perpronoun), perpronoun))
        print("{} Preposition : {}".format(len(prep), prep))
        print("{} Function Word : {}".format(len(func), func))
        print("{} Negation : {}".format(len(negation), negation))
        print('-------------------------Anotasi Positive dan Negatif-----------------------')
        print("{} Negative Annotation : {}".format(len(negAnno), negAnno))
        print("{} Positive Annotation : {}".format(len(posAnno), posAnno))
    pass

def getAnnotation(rowText):
    # =================kata2 positif / neatif
    posAnno = []
    negAnno = []
    doc  = nlp(rowText)
    docBlob = TextBlob(rowText)
    docBlobPolarity = docBlob.sentiment.polarity
    annotaionIns = Annotation(None, negAnno,posAnno)
    for i in range(len(doc)):
        token = doc[i]
        if (token.text in words.words()):  # Cek kata2 yang ada di daftar lata bahasa inggris
            if token.is_punct != True:  # Cek kata yang bukan simbol
                if token.dep_ == 'neg' or (token.tag_ == 'rb' and (token.text == 'not' or token.text == "n't")):
                    if (i + 1) < len(doc):
                        nextWord = doc[i + 1].text
                        word = token.text + ' ' + nextWord
                        word = nlp(word)
                        posState = word._.sentimenter['pos']
                        negState = word._.sentimenter['neg']
                        if posState > negState and negState < 0.15:
                            posAnno.append(word.text)  # tambah kata jika merupakan anotasi positif
                        elif negState > posState and posState < 0.15:
                            negAnno.append(word.text)  # tambah kata jika merupakan anotasi negatif
                        else:
                            word = TextBlob(word.text)
                            senti = word.sentiment.polarity
                            if senti > 0:
                                posAnno.append(str(word))  # tambah kata jika merupakan anotasi positif
                            if senti < 0:
                                negAnno.append(str(word))  # tambah kata jika merupakan anotasi negatif
                else:
                    tokenword = nlp(token.text)
                    posState = tokenword._.sentimenter['pos']
                    negState = tokenword._.sentimenter['neg']
                    if posState > negState:
                        posAnno.append(token.text)  # tambah kata jika merupakan anotasi positif
                    elif negState > posState:
                        negAnno.append(token.text)  # tambah kata jika merupakan anotasi negatif
                    else:
                        word = TextBlob(token.text)
                        senti = word.sentiment.polarity
                        if senti > 0:
                            posAnno.append(token.text)  # tambah kata jika merupakan anotasi positif
                        if senti < 0:
                            negAnno.append(token.text)  # tambah kata jika merupakan anotasi negatif

    annotaionIns = Annotation(None, negAnno, posAnno)
    # print("4. annotation result : {}".format(annotaionIns.__dict__))
    return  annotaionIns

if __name__ == '__main__':
    # dir = "C:\\Users\\Nadian\\PycharmProjects\\TweetSample"
    # read file-file csv di dalam 1 folder untuk bisa diakses
    # fileNameCSVs = []
    # # fileNameCSVs = readFolderCSV(dir)
    # counterDepUser = 0
    # for file in fileNameCSVs:
    #     print(file)
    #     # POSTagSpacy = getPOSTagTextBlob(file)
    #
    # blo = TextBlob("i'm not oky, havng some influenza")
    # correct = blo.correct()
    # print(correct)
    #
    # blob = TextBlob(
    #     "Analytics Vidhya is a great platform to learn data science. \n It helps community through blogs, hackathons, discussions,etc.")
    # for np in blob.noun_phrases:
    #     print(np)


    # blob = TextBlob("Ma classe fonctionne bien, c'est sympathique non ?")
    # tranlated = blob.translate(to='en')
    # print(tranlated)

    # tranlated2 = tranlated.translate(to='fr')
    # print(tranlated2)
    #
    # from pyFeel import Feel
    # test = Feel("Ma classe fonctionne bien, c'est bien, n'est-ce pas?")
    # test.emotions()
    # print(test.emotions())
    # print("cek me")
    # emo = Feel(str(tranlated2)).emotions()
    # print(Feel(str(tranlated2)).emotions())
    #
    # emoIns = Emotion(None, emo['positivity'], emo['joy'], emo['fear'],
    #                  emo['sadness'], emo['angry'], emo['surprise'], emo['disgust'])
    # print(emoIns.__dict__)
    #
    #
    # tranlated3 = tranlated2.translate(to='en')
    # print(tranlated3)

    # blob2 = TextBlob("hmm, I don't require this knowledge.")
    # print(blob2.tags)
    # for word, tag in blob2.tags:
    #     print("textblob : {}, {}" .format(word, tag))
    #     if tag == 'RB' and (word == 'not' or word =="n't"):
    #         print("there's negation")

    print("=====================")
    spacyDoc = nlp("hmm, I don't require this knowledge.")
    for i in range(len(spacyDoc)):
        token = spacyDoc[i]
        print(token.text, token.pos_, token.tag_, token.dep_)
        # if token.dep_ == 'neg':
        #     print("ada negation")
        # if token.tag_ == 'rb' and (token.text == 'not' or token.text =="n't"):
        #     print("INI NEGATION")
        # if token.pos_ == 'INTJ':
        #     print("FILLER ada : {}".format(token.text))

    print("=================sentiment textblob==================")
    sample ="A masked serial killer turns a horror themed amusement park into his own personal playground, terrorizing a group of friends while the rest of the patrons believe that it is all part of the show. College student NATALIE (Forsyth) is visiting her childhood best friend BROOKE (Edwards) and her roommate TAYLOR (Taylor-Klaus). If it was any other time of year these three and their boyfriends might be heading to a concert or bar, but it is Halloween which means that like everyone else they will be bound for HELL FEST - a sprawling labyrinth of rides, games, and mazes that travels the country and happens to be in town. Ever year thousands follow Hell Fest to experience fear at the ghoulish carnival of nightmares. But for one visitor, Hell Fest is not the attraction - it is a hunting ground. An opportunity to slay in plain view of a gawking audience, too caught up in the terrifyingly fun atmosphere to recognize the horrific reality playing out before their eyes. As the body count and frenzied excitement of the crowds continues to rise, he turns his masked face to NATALIE, BROOKE, TAYLOR and their boyfriends who will fight to survive the night."
    sample = "@tarastrong @flotus i was bullied to the point of wanting to take my own life this developed into rampant self harm. My Feel is not good"
    # blob = TextBlob(sample)
    # print(blob.sentiment)
    # for word in blob.words:
    #     word = TextBlob(word)
    #     senti = word.sentiment.polarity
    #     print(word,senti)

    print("=================sentiment NLTK==================")

    # adding extension
    from spacy.tokens import Doc
    from nltk.sentiment.vader import SentimentIntensityAnalyzer

    sent_analyzer = SentimentIntensityAnalyzer()

    # Doc.set_extension("sentimentkan", getter=sentiment_scores)
    Doc.set_extension("sentimenter", getter=sentiment_scores)

    # spacy = nlp(sample)
    # print(spacy._.sentimenter)
    # for i in range(len(spacy)):
    #     word = spacy[i].text
    #     word = nlp(word)
    #     posState = word._.sentimenter['pos']
    #     negState = word._.sentimenter['neg']
    #     neuState = word._.sentimenter['neu']
    #     print(word, posState, negState, neuState)

    # ex1 = nlp("you")
    # print(ex1._.sentimenter)
    #
    # rowText = TextBlob("A masked serial killer turns a horror themed amusement park into his own personal playground, terrorizing a group of friends while the")
    # translated = rowText.translate(to='fr')
    # print("---")
    # myA = getAnnotation(sample)
    # # print(myA.__dict__)
    # word = TextBlob(sample)
    # senti = word.sentiment.polarity
    # cek = []
    # if senti > 0:
    #     cek.append(str(word))  # tambah kata jika merupakan anotasi positif
    # if senti < 0:
    #     cek.append(str(word))
    # print(cek)
