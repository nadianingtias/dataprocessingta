import glob
import pandas as pd
import spacy
import nltk
import pprint

from nltk.tokenize import WordPunctTokenizer
from nltk.corpus import wordnet
from nltk.corpus import words
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from textblob import TextBlob
from spacy.tokens import Doc
from collections import Counter
from pyFeel import Feel

from Model.LangStyle import Emotion
from Model.LangStyle import Sentence
from Model.LangStyle import POSTag
from Model.LangStyle import Annotation
from Model.LangStyle import TweetRow
from Model.LangStyle import UserTweet
from DBRepository.UserTweet import UserTweetRepository
from DBRepository.UserDepressionRepository import UserDepressionRepository as UserDepRepo
from Model.User import User

sent_analyzer = SentimentIntensityAnalyzer()
wpt = WordPunctTokenizer()
nlp = spacy.load('en_core_web_sm')


def sentiment_scores(doc):
    return sent_analyzer.polarity_scores(doc.text)


Doc.set_extension("sentimenter", getter=sentiment_scores)


def readFolderCSV(dir):
    csvDir = glob.glob(dir + '/*.csv');
    filenamecsv = []
    i = 0;
    for file in csvDir:
        i = i + 1
        nameOfFile = file
        space = nameOfFile.find(" ")
        file = nameOfFile[54:]
        file = nameOfFile[61:]
        num = nameOfFile[54:space]
        nameCSV = nameOfFile[space + 1:]
        name = nameCSV[:-11]
        # print("{} | {} || : {} | {} | {}" .format(i, file, num, nameCSV, name))
        filenamecsv.append(nameOfFile)
    print("jumlah CSV : {}".format(i))
    # 2214 records
    return filenamecsv


def getPOSTagNLTK(file):
    # 1. open csv
    df = pd.read_csv(file, encoding='latin-1')
    # 2. read per line
    # for i in range(df.shape[0]):
    for i in range(1):
        sample = df['text'][i]
        print(sample)
        # doc =  nlp(sample)
        # for token in doc:
        #     print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
        #           token.shape_, token.is_alpha, token.is_stop)
        only_recognized_words = []
        tokens = wpt.tokenize(sample)
        for word in tokens:
            if word:  # check if empty string
                if wordnet.synsets(word):
                    only_recognized_words.append(word)  # only keep recognized words
        print(only_recognized_words)
        taggedTokens = nltk.pos_tag(only_recognized_words)

        print(taggedTokens)
        counts = Counter(tag for word, tag in taggedTokens)
        jum = counts
        print(jum)
        lang = [0, 0, 0, 0, 0, 0, 0, 0]
        tem = 0

        if counts['AUX']:
            tem = counts['AUX']
            lang[0] += tem
        print("Auxiliary")
        print(tem)

        if counts['CC']:
            tem = counts['AUX']
            lang[1] += tem
        print("Conjunction")
        print(tem)

        if counts['RB']:
            tem = counts['RB']
            lang[2] += tem
        print("adverb")
        print(tem)

        if counts['RBR']:
            tem = counts['RBR']
            lang[2] += tem
        print("adverb 2")
        print(tem)

        if counts['RBS']:
            tem = counts['RBS']
            lang[2] += tem
        print("adverb 3")
        print(tem)

        if counts['PRP$']:
            tem = counts['PRP$']
            lang[3] += tem
        print("impersonal pron")
        print(tem)

        if counts['PRP']:
            tem = counts['PRP']
            lang[4] += tem
        print("personal pron")
        print(tem)
    # 3. POS perline
    # 4. jumlahkan POS

    # return postags


def getPOSTagSpacy(file):
    # 1. open csv
    df = pd.read_csv(file, encoding='latin-1')
    print(df.shape[0])
    username = file[47:]
    # username = file[48:]
    print("username : {}".format(username))
    arrayTweetRowIns = []  # untuk menyimpan sejumlah tweet da;am 1 CSV user
    # count = 186

    for count in range(df.shape[0]):
        # while count <188:
        # count=count+1
        # =================Sentence sentimen
        created_at = ''
        tokenSize = 0
        negSentimen = 0
        posSentimen = 0
        neuSentimen = 0
        uniSentimen = ''
        multiSentimen = []
        # =================POS tagging
        aux = []
        conj = []
        adv = []
        impronoun = []
        perpronoun = []
        prep = []
        func = []
        negation = []
        filler = []
        # =================kata2 positif / neatif
        posAnno = []
        negAnno = []

        sample = df['text'][count]
        doc = nlp(sample)
        created_at = df['created_at'][count]
        tokenSize = len(doc)
        negSentimen = doc._.sentimenter['neg']
        posSentimen = doc._.sentimenter['pos']
        neuSentimen = doc._.sentimenter['neu']

        if (neuSentimen > (negSentimen + posSentimen)):  # jika skor netral lebih banyak dari kecederungan
            multiSentimen.append('neutral')
            uniSentimen = 'neutral'
        elif negSentimen > posSentimen:  # jika skor kecenderungan lebih besar, dan cenderung negative
            multiSentimen.append('negative')
            uniSentimen = 'negative'
        elif posSentimen > negSentimen:  # jika skor kecenderungan lebih besar, dan cenderung positif
            multiSentimen.append('positive')
            uniSentimen = 'positive'
        else:  # else jika skor kecenderungan lebih besar, tapi skor kecenderungan + - sama
            uniSentimen = 'neutral'
        if negSentimen > posSentimen:  # jika skor kecenderungan lebih besar, dan cenderung negative
            multiSentimen.append('negative')
        if posSentimen > negSentimen:  # jika skor kecenderungan lebih besar, dan cenderung positif
            multiSentimen.append('positive')

        for i in range(len(doc)):
            token = doc[i]
            if (token.text in words.words()):  # Cek kata2 yang ada di daftar lata bahasa inggris
                if token.is_punct != True:  # Cek kata yang bukan simbol
                    if token.dep_ == 'neg' or (token.tag_ == 'rb' and (token.text == 'not' or token.text == "n't")):
                        if (i + 1) < len(doc):
                            nextWord = doc[i + 1].text
                            word = token.text + ' ' + nextWord
                            negation.append(word)  # tambah kata yang berisi negasi
                            word = nlp(word)
                            posState = word._.sentimenter['pos']
                            negState = word._.sentimenter['neg']
                            if posState > negState and negState < 0.15:
                                posAnno.append(token.text)  # tambah kata jika merupakan anotasi positif
                            if negState > posState and posState < 0.15:
                                negAnno.append(token.text)  # tambah kata jika merupakan anotasi negatif
                    else:
                        tokenword = nlp(token.text)
                        posState = tokenword._.sentimenter['pos']
                        negState = tokenword._.sentimenter['neg']
                        if posState > negState:
                            posAnno.append(token.text)  # tambah kata jika merupakan anotasi positif
                        if negState > posState:
                            negAnno.append(token.text)  # tambah kata jika merupakan anotasi negatif

                    if token.dep_ == 'aux':
                        aux.append(token.text)
                        func.append(token.text)
                    if token.dep_ == 'cc' or token.dep_ == 'conj':
                        conj.append(token.text)
                        func.append(token.text)
                    if token.dep_ == 'advcl' or token.dep_ == 'advmod':
                        adv.append(token.text)
                    if token.tag_ == 'PRP$' or token.tag_ == 'WP$':
                        impronoun.append(token.text)
                        func.append(token.text)
                    if token.tag_ == 'PRP' or token.tag_ == 'WP':
                        perpronoun.append(token.text)
                        func.append(token.text)
                    if token.tag_ == 'IN' or token.pos_ == 'ADP':
                        prep.append(token.text)
                        func.append(token.text)
                    if token.pos_ == 'PART' or token.pos_ == 'DET' or token.tag_ == 'MD':
                        func.append(token.text)
                    if token.pos_ == 'INTJ':
                        filler.append(token.text)

        print("======================={}==========================".format(count + 1))
        print('------------------------sentence------------------------')
        print("created_at : {}".format(created_at))
        print("text : {}".format(doc))
        print("ukuran : {}".format(tokenSize))
        print("posSentiment : {}".format(posSentimen))
        print("negSentiment : {}".format(negSentimen))
        print("neuSentiment : {}".format(neuSentimen))
        print("uniSentiment : {}".format(uniSentimen))
        print("multiSentiment : {}".format(multiSentimen))
        print('-------------------------POS tagging-----------------------')
        print("{} Auxiliary : {}".format(len(aux), aux))
        print("{} Conjunction : {}".format(len(conj), conj))
        print("{} Adverb : {}".format(len(adv), adv))
        print("{} Impersonal Pronoun : {}".format(len(impronoun), impronoun))
        print("{} Personal Pronoun : {}".format(len(perpronoun), perpronoun))
        print("{} Preposition : {}".format(len(prep), prep))
        print("{} Function Word : {}".format(len(func), func))
        print("{} Negation : {}".format(len(negation), negation))
        print('-------------------------Anotasi Positive dan Negatif-----------------------')
        print("{} Negative Annotation : {}".format(len(negAnno), negAnno))
        print("{} Positive Annotation : {}".format(len(posAnno), posAnno))

        word_freq = Counter(func)
        common = word_freq.most_common(1)
        # print(common)
        # print("=================================================")

        # build 3 object of each row
        sentenceIns = Sentence(None, created_at, doc, tokenSize, posSentimen, negSentimen, neuSentimen, uniSentimen,
                               multiSentimen)
        POSTagIns = POSTag(None, aux, conj, adv, impronoun, perpronoun, prep, func, negation)
        annotationIns = Annotation(None, negAnno, posAnno)
        # build row object
        tweetRowIns = TweetRow(None, sentenceIns.__dict__, POSTagIns.__dict__, annotationIns.__dict__)
        # insert tweetRow object into array tweetRow
        arrayTweetRowIns.append(tweetRowIns.__dict__)

    userTweetsIns = UserTweet(None, username, arrayTweetRowIns)
    pprint.pprint(userTweetsIns.__dict__)
    pass


def cekSentimen():
    ex1 = nlp("you")
    print(ex1._.sentimenter)
    pass


def cekEnglishWord():
    cek = "would" in words.words()
    # print(cek)
    pass


def justTryMethod():
    cekEnglishWord()
    cekSentimen()
    pass


def getEmotionObj(rowText):
    # GET EMOTION WITH pyFeel
    # 1. translate french with textblob
    rowText = TextBlob(rowText)
    translated = rowText.translate(to='fr')
    # 2. get emotion
    emotionResult = Feel(str(translated)).emotions()
    # print("1. emotion result : {}".format(emotionResult))
    # 3. return emotion
    emotionIns = Emotion(None, emotionResult['positivity'], emotionResult['joy'], emotionResult['fear'],
                         emotionResult['sadness'], emotionResult['angry'], emotionResult['surprise'],
                         emotionResult['disgust'])
    return emotionIns


def getSentence(date, rowText):
    # membentuk objek sentence berisi info csv dan sentimen
    # =================Sentence sentimen
    # 1. sentence info from csv
    doc = nlp(rowText)
    created_at = date
    text = rowText

    # 2. tokensize
    tokenSize = len(doc)

    # 3. sentimen with spacy
    negSentimen = doc._.sentimenter['neg']
    posSentimen = doc._.sentimenter['pos']
    neuSentimen = doc._.sentimenter['neu']

    uniSentimen = ''
    multiSentimen = []
    if (neuSentimen > (negSentimen + posSentimen)):  # jika skor netral lebih banyak dari kecederungan
        multiSentimen.append('neutral')
        uniSentimen = 'neutral'
    elif negSentimen > posSentimen:  # jika skor kecenderungan lebih besar, dan cenderung negative
        multiSentimen.append('negative')
        uniSentimen = 'negative'
    elif posSentimen > negSentimen:  # jika skor kecenderungan lebih besar, dan cenderung positif
        multiSentimen.append('positive')
        uniSentimen = 'positive'
    else:  # else jika skor kecenderungan lebih besar, tapi skor kecenderungan + - sama
        uniSentimen = 'neutral'
    if negSentimen > posSentimen:  # jika skor kecenderungan lebih besar, dan cenderung negative
        multiSentimen.append('negative')
    if posSentimen > negSentimen:  # jika skor kecenderungan lebih besar, dan cenderung positif
        multiSentimen.append('positive')

    # 4. return objek sentence
    sentenceIns = Sentence(None, created_at, text, tokenSize,
                           posSentimen, negSentimen, neuSentimen, uniSentimen, multiSentimen)
    # print("2. sentence result : {}".format(sentenceIns.__dict__))
    return sentenceIns


def getPOSTag(rowText):
    # =================POS tagging
    aux = []
    conj = []
    adv = []
    impronoun = []
    perpronoun = []
    prep = []
    func = []
    negation = []
    filler = []
    doc = nlp(rowText)
    for i in range(len(doc)):
        token = doc[i]
        if (token.text in words.words()):  # Cek kata2 yang ada di daftar lata bahasa inggris
            if token.is_punct != True:  # Cek kata yang bukan simbol
                if token.dep_ == 'neg' or (token.tag_ == 'rb' and (token.text == 'not' or token.text == "n't")):
                    if (i + 1) < len(doc):
                        nextWord = doc[i + 1].text
                        word = token.text + ' ' + nextWord
                        negation.append(word)  # tambah kata yang berisi negasi
                    else:
                        negation.append(token.text)  # tambah kata yang berisi negasi
                if token.dep_ == 'aux':
                    aux.append(token.text)
                    func.append(token.text)
                if token.dep_ == 'cc' or token.dep_ == 'conj':
                    conj.append(token.text)
                    func.append(token.text)
                if token.dep_ == 'advcl' or token.dep_ == 'advmod':
                    adv.append(token.text)
                if token.tag_ == 'PRP$' or token.tag_ == 'WP$':
                    impronoun.append(token.text)
                    func.append(token.text)
                if token.tag_ == 'PRP' or token.tag_ == 'WP':
                    perpronoun.append(token.text)
                    func.append(token.text)
                if token.tag_ == 'IN' or token.pos_ == 'ADP':
                    prep.append(token.text)
                    func.append(token.text)
                if token.pos_ == 'PART' or token.pos_ == 'DET' or token.tag_ == 'MD':
                    func.append(token.text)
                if token.pos_ == 'INTJ':
                    filler.append(token.text)
    POSTagIns = POSTag(None, aux, conj, adv, impronoun, perpronoun,
                       prep, func, negation, filler)
    # print("3. POS tag: {}".format(POSTagIns.__dict__))
    return POSTagIns


def getAnnotation(rowText):
    # =================kata2 positif / neatif
    posAnno = []
    negAnno = []
    doc  = nlp(rowText)
    docBlob = TextBlob(rowText)
    docBlobPolarity = docBlob.sentiment.polarity
    annotaionIns = Annotation(None, negAnno,posAnno)
    for i in range(len(doc)):
        token = doc[i]
        if (token.text in words.words()):  # Cek kata2 yang ada di daftar lata bahasa inggris
            if token.is_punct != True:  # Cek kata yang bukan simbol
                if token.dep_ == 'neg' or (token.tag_ == 'rb' and (token.text == 'not' or token.text == "n't")):
                    if (i + 1) < len(doc):
                        nextWord = doc[i + 1].text
                        word = token.text + ' ' + nextWord
                        word = nlp(word)
                        posState = word._.sentimenter['pos']
                        negState = word._.sentimenter['neg']
                        if posState > negState and negState < 0.15:
                            posAnno.append(word.text)  # tambah kata jika merupakan anotasi positif
                        elif negState > posState and posState < 0.15:
                            negAnno.append(word.text)  # tambah kata jika merupakan anotasi negatif
                        else:
                            word = TextBlob(word.text)
                            senti = word.sentiment.polarity
                            if senti > 0:
                                posAnno.append(str(word))  # tambah kata jika merupakan anotasi positif
                            if senti < 0:
                                negAnno.append(str(word))  # tambah kata jika merupakan anotasi negatif
                else:
                    tokenword = nlp(token.text)
                    posState = tokenword._.sentimenter['pos']
                    negState = tokenword._.sentimenter['neg']
                    if posState > negState:
                        posAnno.append(token.text)  # tambah kata jika merupakan anotasi positif
                    elif negState > posState:
                        negAnno.append(token.text)  # tambah kata jika merupakan anotasi negatif
                    else:
                        word = TextBlob(token.text)
                        senti = word.sentiment.polarity
                        if senti > 0:
                            posAnno.append(token.text)  # tambah kata jika merupakan anotasi positif
                        if senti < 0:
                            negAnno.append(token.text)  # tambah kata jika merupakan anotasi negatif

    annotaionIns = Annotation(None, negAnno, posAnno)
    # print("4. annotation result : {}".format(annotaionIns.__dict__))
    return  annotaionIns


def getArrayTweetCSV(file):
    # 1. open csv
    df = pd.read_csv(file, encoding='latin-1')
    print(df.shape[0])
    arrayTweetRowIns = []  # untuk menyimpan sejumlah tweet dalam 1 CSV user
    count = 159
    for count in range(df.shape[0]):
        # while count<161:
        #     count=count+1
        print("count : {}".format(count))
        rowText = str(df['text'][count])
        timeRowText = df['created_at'][count]
        # 1. koreksi
        blo = TextBlob(rowText)
        corrected = blo.correct()
        rowText = str(corrected)

        # objEmotion = getEmotionObj(rowText)         #make objek emotion
        objSentence = getSentence(timeRowText, rowText)  # make objek sentence
        objPOSTag = getPOSTag(rowText)  # make objek postag
        objAnnotation = getAnnotation(rowText)  # make objek annotation

        # make tweet
        objTweet = TweetRow(None,
                            # objEmotion.__dict__,
                            objSentence.__dict__,
                            objPOSTag.__dict__,
                            objAnnotation.__dict__)
        arrayTweetRowIns.append(objTweet)

    # print("5. array of array")
    # for tweet in arrayTweetRowIns:
    # print("tweet : {}".format(tweet.__dict__))
    return arrayTweetRowIns


def getIDUserDepression(username):
    repoUser = UserDepRepo()
    findUser = repoUser.searchName(username)
    print("usernya : {}".format(findUser.__dict__))
    idUser = None
    objUserFind = User()
    isSuccess = False
    for successresult in findUser:
        print("user : {}".format(successresult))
        isSuccess = True
        objUserFind = objUserFind.build_from_json(successresult)
        idUser = objUserFind._id
        print("id was found : {}".format(idUser))
    if not isSuccess:
        print("User tidak ditemukan di DB")
    return idUser


def findUname(file):
    nameOfFile = file
    space = nameOfFile.find(" ")
    num = nameOfFile[0:space]
    username = nameOfFile[space + 1:-11]
    return username


if __name__ == '__main__':
    import time

    start_time = time.time()

    dir = "C:\\Users\\Nadian\\PycharmProjects\\TweetSampl3"
    # read file-file csv di dalam 1 folder untuk bisa diakses
    fileNameCSVs = []
    fileNameCSVs = readFolderCSV(dir)
    counterDepUser = 0

    for file in fileNameCSVs:
        print(file)
        # POSTagSpacy = getPOSTagSpacy(file)

        # prepare for saving DB
        repoUserTweet = UserTweetRepository()
        arrayTweet = []
        username = findUname(file)
        print("username : {}".format(username))
        idUserTweetIns = getIDUserDepression(username)
        isExist = False
        if idUserTweetIns:
            print("id user ditemukan")
            existUser = repoUserTweet.searchName(username)
            for successresult in existUser:
                print("!!!!! USER SUDAH ADA")
                isExist = True

            if not isExist:
                userTweetIns = UserTweet(idUserTweetIns, username)
                arrayTweet = getArrayTweetCSV(file)
        else:
            userTweetIns = UserTweet(None, username)
            arrayTweet = getArrayTweetCSV(file)
        # userTweetIns = UserTweet(None, username)
        

        for userTweet in arrayTweet:
            userTweetIns.inputTweet(userTweet.__dict__)
            # print(" it is : {}".format(userTweet.__dict__))

        # =========== SAVE to DB
        if not isExist:
            repoUserTweet.create(userTweetIns)
        else:
            repoUserTweet.update(userTweetIns)

    # print(userTweetIns.__dict__)

    # justTryMethod()
    print("--- FINISHED in %s seconds ---" % (time.time() - start_time))
