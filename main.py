from Model.Tweet import Tweet
from DBRepository.DepressionRepository import DepressionRepository as DPrepo
from DBRepository.BipolarRepository import BipolarRepository as BPrepo
from DBRepository.PTSDRepository import PTSDRepository as PTSDrepo
from DBRepository.SADRepository import SADRepository as SADrepo

def load_item_from_collection(repo):
    DBTweets = repo.read()
    isSuccess = False
    tweets = []
    for tweet in DBTweets:
        isSuccess = True
        tem_tweet = Tweet.build_from_json(tweet)
        tweets.append(tem_tweet)
    if not isSuccess:
        print("Belum ada di database")
    return tweets

def cetak_tweet(tweets):
    for tem_tweet in tweets:
        print("ID= {} | username = {} | date = {} | tweet = {} | tweet_id = {} | state = {}".format(tem_tweet._id,
                                                                                                    tem_tweet.username,
                                                                                                    tem_tweet.date,
                                                                                                    tem_tweet.text,
                                                                                                    tem_tweet.tweet_id,
                                                                                                    tem_tweet.state))

def load_and_print_raw_data():
    print("Load Items in 'Depression' Collection")
    depr_repo = DPrepo()
    deprTweets = load_item_from_collection(depr_repo)
    cetak_tweet(deprTweets)

    print("Load Items in 'Bipolar' Collection")
    bipolar_repo = BPrepo()
    bipolarTweets = load_item_from_collection(bipolar_repo)
    cetak_tweet(bipolarTweets)

    print("Load Items in 'PTSD' Collection")
    ptsd_repo = PTSDrepo()
    ptsdTweets = load_item_from_collection(ptsd_repo)
    cetak_tweet(ptsdTweets)

    print("Load Items in 'SAD' Collection")
    sad_repo = SADrepo()
    sadTweets = load_item_from_collection(sad_repo)
    cetak_tweet(sadTweets)


def distinct_depression_tweet(DPrepo):
    distTweet = []
    tweets = load_item_from_collection(DPrepo)
    tweets2 = tweets

def progress1():
    print("Hello")
    # load_and_print_raw_data()
    print("Load Items in 'Depression' Collection")
    depr_repo = DPrepo()
    deprTweets = load_item_from_collection(depr_repo)
    cetak_tweet(deprTweets)

    print("Load Items in 'Bipolar' Collection")
    bipolar_repo = BPrepo()
    bipolarTweets = load_item_from_collection(bipolar_repo)
    # cetak_tweet(bipolarTweets)

    print("Load Items in 'PTSD' Collection")
    ptsd_repo = PTSDrepo()
    ptsdTweets = load_item_from_collection(ptsd_repo)
    # cetak_tweet(ptsdTweets)

    print("Load Items in 'SAD' Collection")
    sad_repo = SADrepo()
    sadTweets = load_item_from_collection(sad_repo)
    # cetak_tweet(sadTweets)

    print("Distinct Tweet in 'Depression' Collection")
    distinct = depr_repo.distinctRawData()
    i = 0
    for dist in distinct:
        i = i + 1
        print(i)
        print(dist)

if __name__ == '__main__':
    progress1()


#2nd week nov - have been distinct on collection bipolarTweet_distinct, depressionTweet_distinct, ptsdTweet_distinct, sadTweet_distinct
#2nd week nov - have been distinct user on collection userBipolar, userDepression, userPTSD, userSAD,
#userAll - collect all user

