import re
from nltk.corpus import sentiwordnet as swn


regex_str = [
    # r'<[^>]+>',  # HTML tags
    r'(?:@[\w_]+)',  # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)",  # hash-tags
    # r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+',  # URLs
    # r'(?:(?:\d+,?)+(?:\.?\d+)?)',  # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])",  # words with - and '
    r'(?:[\w_]+)',  # other words
    # r'(?:\S)'  # anything else
]
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
def tokenize(s):
    return tokens_re.findall(s)
def preprocess(param, lowercase=False):
    tokens = tokenize(param)
    if lowercase:
        tokens = [token.lower() for token in tokens]
    return tokens

# tweet = "b'RT @EJBrand: Glimpse at a childhood in Victorian London: in the 1890s,https://t.co/sX9dP9z1sv https://t.co\xe2\x80\xa6 a little girl in Bethnal Green made this cherished doll by clothing\xe2\x80\xa6'"
# print(tweet)
# cleanlist = preprocess(tweet, True)
# print(preprocess(tweet))

def trial():
    from nltk.corpus import stopwords
    stop = stopwords.words("english")
    # clean = ' '.join(cleanlist)
    # print(clean)

if __name__ == '__main__':

    hasil = swn.senti_synsets('happy.a.03')
    print("hasil {}".format(hasil))
    print(hasil)
    list = list(hasil)
    print(list)

    for h in hasil:
        print("h : {}".format(h))

    # trial()

    import pandas as pd
    df = pd.read_csv('20 hchazan_tweets.csv', encoding='latin-1')

    print('--print basic info of data')
    print(df.info())
    print(df.shape)
    print('---print the head/tail of data---')
    short_data = df.head();
    print(short_data['text'])
    print(df.shape[0])
    print('----------------')

    cleanByRegex = preprocess(short_data['text'][0], True)
    print("regex preprocess")
    print(cleanByRegex)

    print('---------------------')
    from nltk.tokenize import WordPunctTokenizer
    from nltk.corpus import wordnet
    wpt = WordPunctTokenizer()
    only_recognized_words = []
    my_strings = cleanByRegex
    # wordList = re.sub("[^\w]", " ", my_strings).split()
    num = ['1', '2', '3', '4', '5', '6']
    for h in num:
        tokens2 = wpt.tokenize(h)
    for s in my_strings:
        tokens = wpt.tokenize(s)
        if tokens:  # check if empty string
            for t in tokens:
                if t not in tokens2:
                    if wordnet.synsets(t):
                        only_recognized_words.append(t)  # only keep recognized words

    print("recognized sentiword")
    print(only_recognized_words)

    print('--- other source ---')
    #untuk memecah perkata
    from nltk.tokenize import word_tokenize
    lowercase = short_data['text'][0].lower()
    tokenized_word = word_tokenize(lowercase)
    print(tokenized_word)

    #untuk mencari frekuensi kemunculan kata
    from nltk.probability import FreqDist
    fdist = FreqDist(tokenized_word)
    print(fdist)
    print(fdist.most_common(2))

    # Frequency Distribution Plot
    import matplotlib.pyplot as plt

    fdist.plot(30, cumulative=False)
    # plt.show()

    #removing stopword
    # from nltk.corpus import stopwords
    #
    # stop_words = set(stopwords.words("english"))
    # print(stop_words)
    #

    # Stemming (mengambil kata dasar saja)
    from nltk.stem import PorterStemmer
    from nltk.tokenize import sent_tokenize, word_tokenize

    ps = PorterStemmer()
    filtered_sent =  tokenized_word
    stemmed_words = []
    for w in filtered_sent:
        stemmed_words.append(ps.stem(w))

    print("Filtered Sentence:", filtered_sent)
    print("Stemmed Sentence:", stemmed_words)

    # Lexicon Normalization
    # performing stemming and Lemmatization
    from nltk.stem.wordnet import WordNetLemmatizer
    lem = WordNetLemmatizer()
    from nltk.stem.porter import PorterStemmer

    stem = PorterStemmer()
    word = "i flew last year"
    print("Lemmatized Word:", lem.lemmatize(word, "v"))
    print("Stemmed Word:", stem.stem(word))

    print("===================================")
    #POS-tagging
    import nltk as nltk
    sent = short_data['text'][0]
    tokens = word_tokenize(sent)
    print(tokens)
    nltk.pos_tag(tokens)
