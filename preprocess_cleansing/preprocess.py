import pandas as pd

df = pd.read_csv('20 hchazan_tweets.csv', encoding='latin-1')

print('--print basic info of data')
print(df.info())
print(df.shape)

print('---print the head/tail of data---')
print(df.head())
print('----------------')
# print(df.tail())


# step 1 stop word removal
short_data = df.head()
from nltk.corpus import stopwords
stop = stopwords.words("english")

print(short_data['text'])
print('-------Remove Stop Word--------')
# short_data['Step1_SentimentText'] = short_data['text'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))
# print(short_data['Step1_SentimentText'])

from nltk.tokenize import word_tokenize
print(stop)
print(short_data['text'][0])
word_tokens = word_tokenize(short_data['text'][0])
filtered_sentence = [w for w in word_tokens if not w in stop]
print(filtered_sentence)
