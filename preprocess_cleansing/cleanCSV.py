import pandas as pd
import spacy
import re
import glob
from nltk.corpus import wordnet
from nltk.corpus import words
nlp = spacy.load('en_core_web_sm', disable=['parser','ner'])

def unicodetoascii(text):
    TEXT = (text.
            replace('\\xF0\\x9F\\x98\\x81', "joying ").
            replace('\\xF0\\x9F\\x98\\x82', "joying ").
            replace('\\xF0\\x9F\\x98\\x83', "joying ").
            replace('\\xf0\\x9f\\x98\\x83', "joying ").
            replace('\\xF0\\x9F\\x98\\x84', "joying ").
            replace('\\xF0\\x9F\\x98\\x85', "joying ").
            replace('\\xF0\\x9F\\x98\\x86', "joying ").
            replace('\\xF0\\x9F\\x98\\x89', "joying ").
            replace('\\xF0\\x9F\\x98\\x8A', "joying ").
            replace('\\xF0\\x9F\\x98\\x8B', "joying ").
            replace('\\xF0\\x9F\\x98\\x8C', "relieved ").
            replace('\\xF0\\x9F\\x98\\x8D', "loving ").
            replace('\\xF0\\x9F\\x98\\x8F', "smirking ").
            replace('\\xF0\\x9F\\x98\\x92', "bored ").
            replace('\\xF0\\x9F\\x98\\x93', "bored ").
            replace('\\xF0\\x9F\\x98\\x94', "sad ").
            replace('\\xF0\\x9F\\x98\\x96', "sad ").
            replace('\\xF0\\x9F\\x98\\x98', "loving ").
            replace('\\xF0\\x9F\\x98\\x9A', "loving ").
            replace('\\xF0\\x9F\\x98\\x9C', "funny ").
            replace('\\xF0\\x9F\\x98\\x9D', "funny ").
            replace('\\xF0\\x9F\\x98\\x9E', "sad ").
            replace('\\xF0\\x9F\\x98\\xA0', "angry ").
            replace('\\xF0\\x9F\\x98\\xA1', "angry ").
            replace('\\xF0\\x9F\\x98\\xA2', "sad ").
            replace('\\xF0\\x9F\\x98\\xA3', "sad ").
            replace('\\xF0\\x9F\\x98\\xA4', "angry ").
            replace('\\xF0\\x9F\\x98\\xA5', "sad ").
            replace('\\xF0\\x9F\\x98\\xA8', "sad ").
            replace('\\xF0\\x9F\\x98\\xA9', "sad ").
            replace('\\xF0\\x9F\\x98\\xAA', "tired ").
            replace('\\xF0\\x9F\\x98\\xAB', "tired ").
            replace('\\xF0\\x9F\\x98\\xAD', "sad ").
            replace('\\xF0\\x9F\\x98\\xB0', "sad ").
            replace('\\xF0\\x9F\\x98\\xB1', "surprised ").
            replace('\\xF0\\x9F\\x98\\xB2', "surprised ").
            replace('\\xF0\\x9F\\x98\\xB3', "surprised ").
            replace('\\xF0\\x9F\\x98\\xB4', "surprised ").
            replace('\\xF0\\x9F\\x98\\xB7', "sick ").
            replace('\\xF0\\x9F\\x98\\xB8', "joying ").
            replace('\\xF0\\x9F\\x98\\xB9', "joying ").
            replace('\\xF0\\x9F\\x98\\xBA', "joying ").
            replace('\\xF0\\x9F\\x98\\xBB', "joying ").
            replace('\\xF0\\x9F\\x98\\xBC', "sad ").
            replace('\\xF0\\x9F\\x98\\xBD', "joying ").
            replace('\\xF0\\x9F\\x98\\xBE', "angry ").
            replace('\\xF0\\x9F\\x98\\xBF', "sad ").
            replace('\\xF0\\x9F\\x99\\x80', "surprised ").
            replace('\\xF0\\x9F\\x99\\x85', "deny ").
            replace('\\xF0\\x9F\\x99\\x86', "joying ").
            replace('\\xF0\\x9F\\x99\\x8F', "sorry ").
            replace('\\xf0\\x9f\\x91\\x8f', "clapping ").
            replace('\\xF0\\x9F\\x91\\x8C', "agree ").
            replace('\\xF0\\x9F\\x91\\x8D', "agree ").
            replace('\\xF0\\x9F\\x91\\x8E', "disagree ").
            replace('\\xF0\\x9F\\x8E\\x83', "halloween ").
            replace('\\xf0\\x9f\\x8e\\x84', "christmas ").
            replace('\\xf0\\x9f\\x8e\\x85', "christmas ").
    		replace('\\xe2\\x80\\x99', "'").
            replace('\\xc3\\xa9', 'e').
            replace('\\xe2\\x80\\x90', '-').
            replace('\\xe2\\x80\\x91', '-').
            replace('\\xe2\\x80\\x92', '-').
            replace('\\xe2\\x80\\x93', '-').
            replace('\\xe2\\x80\\x94', '-').
            replace('\\xe2\\x80\\x94', '-').
            replace('\\xe2\\x80\\x98', "'").
            replace('\\xe2\\x80\\x9b', "'").
            replace('\\xe2\\x80\\x9c', '"').
            replace('\\xe2\\x80\\x9c', '"').
            replace('\\xe2\\x80\\x9d', '"').
            replace('\\xe2\\x80\\x9e', '"').
            replace('\\xe2\\x80\\x9f', '"').
            replace('\\xe2\\x80\\xa6', '...').#
            replace('\\xe2\\x80\\xb2', "'").
            replace('\\xe2\\x80\\xb3', "'").
            replace('\\xe2\\x80\\xb4', "'").
            replace('\\xe2\\x80\\xb5', "'").
            replace('\\xe2\\x80\\xb6', "'").
            replace('\\xe2\\x80\\xb7', "'").
            replace('\\xe2\\x81\\xba', "+").
            replace('\\xe2\\x81\\xbb', "-").
            replace('\\xe2\\x81\\xbc', "=").
            replace('\\xe2\\x81\\xbd', "(").
            replace('\\xe2\\x81\\xbe', ")")

                 )
    return TEXT
regex_str = [
    # r'<[^>]+>',  # HTML tags
    r'(?:@[\w_]+)',  # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)",  # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+',  # URLs
    # r'(?:(?:\d+,?)+(?:\.?\d+)?)',  # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])",  # words with - and '
    r"(?:[\w_]+)",  # other words
    # r'(?:\S)'  # anything else
]
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
def tokenize(s):
    return tokens_re.findall(s)
def preprocess(param, lowercase=False):
    tokens = tokenize(param)
    if lowercase:
        tokens = [token.lower() for token in tokens]
    return tokens

def readFolderCSV(dir):
    csvDir = glob.glob(dir + '/*.csv');
    filenamecsv = []
    i = 0;
    for file in csvDir:
        i=i+1
        nameOfFile = file
        space = nameOfFile.find(" ")
        file = nameOfFile[54:]
        file = nameOfFile[61:]
        num = nameOfFile[54:space]
        nameCSV = nameOfFile[space+1:]
        name = nameCSV[:-11]
        # print("{} | {} || : {} | {} | {}" .format(i, file, num, nameCSV, name))
        filenamecsv.append(nameOfFile)
        # singleCSV = csv.reader(open(file, 'r', errors='ignore'), delimiter=',')
        # for row in singleCSV:
        #     print(i)
        #     i = i + 1
            # newTweet = Tweet(None, row[1], row[2], row[5], row[9],state)
            # insertTweet(repoTweet, newTweet)
            # print("ok")
    print(i)
    # 2214 records
    return filenamecsv
def cleansingCSVTweet(filename):
    print("will process", filename)
    # df = pd.read_csv(os.path.join(os.path.dirname(__file__), filename))
    df = pd.read_csv(filename, encoding='latin-1')
    for i in range(df.shape[0]):
        sample = df['text'][i][1:]
        # print("==================1. unicode to ascii ==================")
        # print(sample)
        ascii = unicodetoascii(sample)
        # print(ascii)
        # print("==================2. ascii preprocess regex ==================")
        cleanByRegex = preprocess(ascii, True)
        # print("==================3. join hasil preprocess regex (list -> string)==================")
        joinCleanTweet = ' '.join(cleanByRegex)
        data = joinCleanTweet
        # print(data)
        print(i)
        df['text'][i] = data
    print(filename)
    df.to_csv(filename)


def myPreprocess(textList):
    teks = ' '.join(textList)
    print(teks)
    result= []
    spacy = nlp(teks)
    for i in range(len(spacy)):
        token = spacy[i]
        lemma = token.lemma_
        print(spacy[i])
        print(lemma)
    #     print(lemma)
    #     print(type(lemma))
        text = str(token)
        if lemma == '-PRON-':
            print("pron")
            result.append(text)
        elif lemma in words.words() and len(lemma) > 1:
            print("in word")
            result.append(text)
    #     elif text == 'http' or text == 'www':
            # result.append()
        elif (lemma[0] == '@' or lemma[0] == 'r' or text == 'i' or text == 'a'):
            print("mention, or hashtag, or A, a")
            result.append(text)
        elif wordnet.synsets(lemma) and len(lemma) > 1:
            print("in wordnet")
            if lemma != 'http' and lemma != 'www':
                result.append(text)
        elif lemma == 'iam':
            result.append('i')
            result.append('am')
    #
    #
    #     print(spacy[i])


    print("len : {}".format(len(result)))
    return result


if __name__ == '__main__':
    # dir = "C:\\Users\\Nadian\\PycharmProjects\\mypython\\TweetPerUser"
    # # read file-file csv di dalam 1 folder untuk bisa diakses
    # fileNameCSVs = []
    # fileNameCSVs = readFolderCSV(dir)
    # for file in fileNameCSVs:
    #     # print(file)
    #     cleansingCSVTweet(file)
    #
    # filename = '20 hchazan_tweets.csv'
    # print(filename)
    # df = pd.read_csv(filename, encoding='latin-1')
    # short_data = df;
    # print(df.shape)
    #
    # print('original')
    # for i in range(df.shape[0]):
    #     sample = df['text'][i][1:]
    #     # print(sample)
    #     # df['text'][i] = sample
    #     # print("==================1. unicode to ascii ==================")
    #     ascii = unicodetoascii(sample)
    #     # print(ascii)
    #     # print("==================2. ascii preprocess regex ==================")
    #     cleanByRegex = preprocess(ascii, True)
    #     # print(cleanByRegex)
    #     joinCleanTweet = ' '.join(cleanByRegex)
    #     # print(joinCleanTweet)
    #     data = joinCleanTweet
    #     # df['text'][i] = data
    #     # df.loc[:,('text',i)] = data
    #     # df.loc.__setitem__((slice(None), ('text', i)), data)

        # print(sample)
    # df.to_csv('contoh.csv')

    #JANUARI - i have to rearrange my preprocessing technique
    import unicodedata

    # raw_text = "b'RT you're wouldn't @pienar: http:\\www.google.com \\xF0\\x9F\\x98\\x81 texting my crush \xe2\x80\x9chey infant\xe2\x80\x9d instead of baby so they know i\xe2\x80\x99m smart and regularly use my thesaurus, and i don't care anymore"
    # raw_text = "b'RT @buyi_the_human: Here\xe2\x80\x99s Groot on vacation to brighten up ur day \xf0\x9f\x98\xad\xf0\x9f\x98\xad\xf0\x9f\x98\xad https://t.co/yTYEFNBV4s'"
    # raw_text = "\xe2\x80\x99"
    # print(type(raw_text))
    # print("raw text : {}".format(raw_text))
    # raw_text = unicodetoascii(raw_text)

    # convert_text = unicodedata.normalize('NFKD', raw_text).encode('ascii', 'ignore')
    # print(type(convert_text))
    # print("after convert normalize: {}".format(convert_text))
    # str_text = convert_text.decode("utf-8")
    # print(type(str_text))
    # print("after decode: {}".format(str_text))

    # ascii = unicodetoascii(raw_text)
    # print(type(ascii))
    # print("after unicodetoascii: {}".format(ascii))
    # text2 = preprocess(ascii, True)
    # print("regex preprocess: {}".format(text2))
    #
    # resultmypreprocess = myPreprocess(text2)
    # print("after mypreprocess : {}".format(resultmypreprocess))
    #
    # print("raw text : {}".format(raw_text))
    # lastresult = ' '.join(resultmypreprocess)
    # print("last result : {}".format(lastresult))

    print(wordnet.synsets('sad'))

    print(wordnet.synsets.frame_ids('sad'))
