from nltk.tokenize import WordPunctTokenizer
from nltk.corpus import wordnet

print(wordnet)
# my_list_of_strings = "b'RT @EJBrand: Glimpse at a childhood in Victorian London: in the 1890s, a little girl in Bethnal Green made this cherished doll by clothing\xe2\x80\xa6"  # populate list before using
my_list_of_strings = ["Nadia is happy /100/6/5994 @nadianingtias \xf0\x9f\x91\x8f\xf0\x9f\x8f\xbb\xf0\x9f"]

wpt = WordPunctTokenizer()
only_recognized_words = []

num = ['1','2','3','4','5', '6']
for h in num:
    tokens2 = wpt.tokenize(h)
for s in my_list_of_strings:
    tokens = wpt.tokenize(s)
    if tokens:  # check if empty string
        for t in tokens :
            if t not in tokens2:
                if wordnet.synsets(t):
                    only_recognized_words.append(t)  # only keep recognized words

print(only_recognized_words)

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

example_sent = "This is a sample sentence, showing off the stop words filtration 8928 my. "

stop_words = set(stopwords.words('english'))
# print(stop_words)
word_tokens = word_tokenize(example_sent)

filtered_sentence = [w for w in word_tokens if not w in stop_words]

filtered_sentence = []

for w in word_tokens:
    if w not in stop_words:
        filtered_sentence.append(w)

print(word_tokens)
print(filtered_sentence)


