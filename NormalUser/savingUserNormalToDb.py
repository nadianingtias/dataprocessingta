import time
import glob
import pandas as pd
import csv

from DBRepository.UserAllRepository import UserAllRepository as UserAllRepo
from Model.User import User
from learn.Tweet import Tweet
from learn.TweetRepository import TweetRepository

def insertTweet(repo, new_object):
    repo.create(new_object)
    # proses membaca entry dengan param ID yang baru dimasukkan
    db_datas = repo.read(id=new_object._id)
    for p in db_datas:
        data_from_db = Tweet.build_from_json(p)
        print("New Item have been inserted = {}".format(data_from_db.get_as_json()))


def importTweetNorm(repoTweet,dir):
    state="NORMAL"
    print(state)
    csvDir = glob.glob(dir + '/*.csv');
    i = 0;
    for file in csvDir:
        # name = file.split(', ', 7)
        print(file)
        singleCSV = csv.reader(open(file, 'r', errors='ignore'), delimiter=',')
        for row in singleCSV:
            print(i)
            i = i + 1
            print(row)
            newTweet = Tweet(None, row[1], row[2], row[5], row[9],state)
            insertTweet(repoTweet, newTweet)
            print("ok")

if __name__ == '__main__':
    start_time = time.time()
    dir = "C:\\Users\\Nadian\\PycharmProjects\\normal"
    tweetRepo = TweetRepository()
    print("--")
    importTweetNorm(tweetRepo, dir)
    print("---")

    print("--- FINISHED in %s seconds ---" % (time.time() - start_time))

