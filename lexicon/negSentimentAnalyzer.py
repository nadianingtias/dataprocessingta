import pprint

from DBRepository.UserTweetNormalRepository import UserTweetNormalRepository as UserTweetNormRepo
from DBRepository.UserTweetRepository import UserTweetRepository as UserTweetDepRepo
from DBRepository.UserDepressionRepository import UserDepressionRepository as UserDepRepo
from Model.LangStyle import UserTweet
from Model.LangStyle import Annotation
from Model.LangStyle import TweetRow
from DBRepository.WordList_depression_Repository import WordList_depressionRepository as wordlist_dep_repo
from Model.WordList import WordList as WordList

from Model.User import User



if __name__ == '__main__':
    #START FROM THIS
    userDepressionRepo = UserTweetDepRepo()
    trainDepression = userDepressionRepo.read()

    counterUser = 0
    # 1. read depresi user total 38 user, read normal user total 38
    NegWordList = []
    for trainUser in trainDepression:
        counterUser += 1
        userTweets = trainUser['tweets']
        # print(userTweets)
        # 2. masing2 user punya banyak tweets, tweets per user dibaca per rownya
        for tweet in userTweets:
            # print(tweet['annotation'])
            # 3. masing2 row tweet punya atribut annotation,
            annotationOfEachTweet = tweet['annotation']
            # print(annotationOfEachTweet['negAnno'])
            NegWord = annotationOfEachTweet['negAnno']
            for Word in NegWord:
                if Word != None:
                    # print(Word)
                    # NegWordList.append(Word)
                    NegWordList.append(Word)

    counter = 0
    wordList_depRepo = wordlist_dep_repo()
    state = "DEPRESSION"
    wordObject = WordList()
    for word in NegWordList:
        print(type(word))
        counter = counter + 1
        print(word)
        existWord = wordList_depRepo.searchWord(word)
        isExist = False
        count = 0
        for successresult in existWord:
            print("akan tambah counter")
            isExist = True
            objWord = wordObject.build_from_json(successresult)
            idWord = objWord._id
            countWord = objWord.count + 1
        #jika ada di dalam DB
            if idWord :
                # //update
                print("updating word dengan id : {} ".format(idWord))
                updateWord = WordList(idWord, word, countWord, state)
                wordList_depRepo.update(updateWord)
        #jika tidak ada di dalam DB
        if not isExist:
            print("new word!!")
            isExist = False
            count = 1
            newWord = WordList(None, word, count, state)
            wordList_depRepo.create(newWord)

    print("{} words of negative word list FOUND".format(counter))
