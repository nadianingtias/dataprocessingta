import glob
import pandas as pd
from textblob import TextBlob

def readFolderCSV(dir):
    csvDir = glob.glob(dir + '/*.csv')
    filenamecsv = []
    i = 0;
    for file in csvDir:
        i = i + 1
        nameOfFile = file
        space = nameOfFile.find(" ")
        file = nameOfFile[54:]
        file = nameOfFile[61:]
        num = nameOfFile[54:space]
        nameCSV = nameOfFile[space + 1:]
        name = nameCSV[:-11]
        # print("{} | {} || : {} | {} | {}" .format(i, file, num, nameCSV, name))
        filenamecsv.append(nameOfFile)
    print("jumlah CSV : {}".format(i))
    # 2214 records
    return filenamecsv


def goTranslate(text):
    blob = TextBlob(text)
    trans =  blob.translate(to='en')
    print("output translate : {}".format(trans))
    print(type(trans))
    return trans


if __name__ == '__main__':
    dir ="C:\\Users\\Nadian\\OneDrive\\Dokumen Kuliah\\TA\\DATA\\DATA lexicon"

    fileNameCSVs = []
    fileNameCSVs = readFolderCSV(dir)

    # for file in fileNameCSVs:
    #     df = pd.read_csv(file, encoding='latin-1')
    #     # 2. read per line
    #     # for i in range(df.shape[0]):
    #     for i in range(100):
    #         print(i)
    #         sample = (df['word'][i])
    #         print("input: {}".format(sample))
    #         translated = goTranslate(sample)
    #     # for row in df:
    #     #     print(row)

    sample = "ablation"
    blob= TextBlob(sample)
    try:
        trans =  blob.translate(from_lang='fr',to='en')
    except:
        trans = str(blob)
        print("cant be translated")
    print(trans)