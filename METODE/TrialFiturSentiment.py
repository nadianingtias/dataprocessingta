import random
import pprint

from DBRepository.UserTweet import UserTweetRepository
from DBRepository.UserTweetNormalRepository import UserTweetNormalRepository as UserTweetNormRepo
from Model.LangStyle import UserTweet
from Model.LangStyle import Annotation
from Model.LangStyle import TweetRow
from Model.LangStyle import Sentence
import math
from bson.objectid import ObjectId

#METHOD untuk mengembalikan array of zero
def initArrayNol(jumIndex):
    array = []
    for i in range(jumIndex):
        array.append(0)
    return array

#METHOD untuk menormalisasi 'angka' di dalam range 'jumlah'
def normalize(angka, jumlah):
    return angka/jumlah

if __name__ == '__main__':
    # A. proses train user NORMAL
    userNormalRepo = UserTweetNormRepo()
    trainNormal = userNormalRepo.read()
    # B. proses train user DEPRESSION
    userDepressionRepo = UserTweetRepository()
    trainDepression = userDepressionRepo.read()
    # 1. read normal user total 35, read normal user total 26
    counterUser = 0
    usernames = []
    userSentiments = []
    for trainUser in trainDepression:
        counterUser += 1
        if counterUser > 35:
            break
        print("============================== user : {}".format(counterUser))
        print(trainUser['username'])
        usernames.append(trainUser['username'])
        #build object
        userTweetTrain = UserTweet(trainUser['_id'], trainUser['username'], trainUser['tweets'])
        # 2. for user ambil array tweetnya
        tweetarray = userTweetTrain.tweets
        countrow = 0
        countPosAno = 0
        countNegAno = 0
        sentimentPos = 0
        sentimentNeg = 0
        sentimentNeu = 0
        fluktuasi = []
        for tweet in tweetarray:
            countrow += 1
            if counterUser > 35:
                break
            #==build object tweetrow
            tweetRow = TweetRow.build_from_json(tweet)
            #==build object annotation
            annotation = Annotation.build_from_json(tweetRow.annotation)
            #==build object sentence
            # pprint.pprint(tweetRow.sentence)
            sentence = Sentence.build_from_json(tweetRow.sentence)
            #=========CALCULATE MULTISENTIMENT
            multi = sentence.multiSentiment
            # cpos = cneg = cneu = cunk = csum = 0
            cMulti = initArrayNol(5) # [ cpos, cneg, cneu, cunk, csum ]
            for sent in multi:
                if sent == 'positive':
                    # cpos += 1
                    cMulti[0] +=1
                    cMulti[4] +=1
                elif sent == 'negative':
                    # cneg += 1
                    cMulti[1] +=1
                    cMulti[4] +=1
                elif sent == 'neutral':
                    # cneu += 1
                    cMulti[2] +=1
                    cMulti[4] += 1
                else:
                    # cunk += 1
                    cMulti[3]+=1
                    cMulti[4] += 1
            # csum = cpos + cneg + cneu + cunk
            # bagpos = bagneg = bagneu = bagunk = 0
            bagMulti = initArrayNol(4) # [bagpos, bagneg, bagneu, bagunk]
            # print("csum : {}".format(csum))
            print("csum : {}".format(cMulti[4]))
            # if csum != 0:  #cek agar tida kerror division by zero
            #     bagpos = cpos / csum
            #     bagneg = cneg / csum
            #     bagneu = cneu / csum
            #     bagunk = cunk / csum
            #     resultbag = [bagpos, bagneg, bagneu, bagunk]
            #     pprint.pprint(resultbag)
            if cMulti[4] !=0: #cek agar tida kerror division by zero
                for i in range(len(bagMulti)):
                    bagMulti[i] = cMulti[i] / cMulti[4]
                pprint.pprint(bagMulti)
            else:
                print("hayo")

            #========CALCULATE UNISENTIMENT
            if sentence.uniSentiment == 'positive':
                # print("positif")
                sentimentPos += 1
                # fluktuasi.append(1)
            elif sentence.uniSentiment == 'negative':
                # print("negatif")
                sentimentNeg += 1
                # fluktuasi.append(-1)
            elif sentence.uniSentiment == 'neutral':
                sentimentNeu +=1
                # DENGAN MULTISENTIMENT
                if bagMulti[2] > 0.5:
                    print("netral")
                    print(bagMulti[2])
                    sentimentNeu += 1
                elif bagMulti[1] > 0 :
                    sentimentNeg +=1
                elif bagMulti[0] > 0 :
                    sentimentPos += 1
                fluktuasi.append(0)
            else:
                print("we all dont know")


            # print("sentimene pos:{} neg:{} neu:{}".format(sentimentPos, sentimentNeg, sentimentNeu))
        userSentiment = [sentimentPos, sentimentNeg, sentimentNeu]
        userSentiments.append(userSentiment)
        pprint.pprint(userSentiment)

        # TOTAL SEMUA + - 0
        sumsenti = sentimentPos + sentimentNeg + sentimentNeu
        # HANYA MELIBATKAN + -
        # sumsenti = sentimentPos + sentimentNeg

        if sumsenti !=0:
            print(sumsenti)
            for i in range(len(userSentiment)):
                userSentiment[i] = normalize(userSentiment[i], sumsenti)
            pprint.pprint("user sent : {}".format(userSentiment))


    # pprint.pprint(userSentiments)
#DATA VISUALIZATION - FEATURE +/- wordcount
    import matplotlib.pyplot as plt

    # plt.title("Distribusi jumlah sentiment pada user training NORMAL")
    plt.title("Distribusi jumlah sentiment pada user training DEPRESI")
    plt.xlabel("masing user")
    featureClass = usernames
    plt.ylabel("Jumlah tweet yang digolongkan ke dalam sentimen + - 0")
    posPlot = []
    negPlot = []
    neuPlot = []
    selisih = []
    for userSent in userSentiments:
        posPlot.append(userSent[0])
        negPlot.append(userSent[1])
        neuPlot.append(userSent[2])
        pprint.pprint(userSent)
        selisih.append(math.fabs(userSent[0]-userSent[1]))

    # attr_pos_normal = counterBayesPosAno.copy()
    # attr_pos_depression = counterBayesPosAnoDep.copy()
    # posPlot.sort()
    # negPlot.sort()
    # posPlot.sort(reverse=True)
    # negPlot.sort(reverse=True)
    # neuPlot.sort()
    selisih.sort(reverse=True)
    print("highest range  {}".format(selisih[0]))
    print("lowest range  {}".format(selisih[34]))
    plt.plot(featureClass, posPlot, color='g', label='POSITIF')
    plt.plot(featureClass, negPlot, color ='r', label='NEGATIF')
    # plt.plot(featureClass, neuPlot, color='b', label='NEUTRAL')
    axes = plt.gca()
    axes.set_ylim([0, 0.5])
    # plt.plot(featureClass, attr_pos_depression, color='orange', label='DEPRESSION')
    plt.legend(loc='upper left')
    plt.show()