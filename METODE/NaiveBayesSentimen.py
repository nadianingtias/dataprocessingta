import random

from DBRepository.UserTweet import UserTweetRepository
from DBRepository.UserTweetNormalRepository import UserTweetNormalRepository as UserTweetNormRepo
from Model.LangStyle import UserTweet
from Model.LangStyle import Annotation
from Model.LangStyle import TweetRow
import math
from bson.objectid import ObjectId

def normalize(angka, jumlah):
    return angka/jumlah


def cariKelas(value, jumKelasAtribut):
    print("value : {}".format(value))
    int = 0
    # print("gugu")
    # print(type(int))
    kelas = value / (1 / jumKelasAtribut)  # misal : value = 0.2. 0.2/(1/5) = 1
    if kelas == 0:
        kelas = 0;
    elif (kelas % 1) > 0:
        kelas = math.floor(kelas-(kelas%1) + 1 -1)
    else:
        kelas = math.floor(kelas - 1)


    print("Masuk kelas : {}".format(kelas))
    # print(type(kelas))
    return kelas


def initArrayNol(jumIndex):
    array = []
    for i in range(jumIndex):
        array.append(0)
    return array


class BayesObject(object):
    """A class for storing Project related information"""
    # constructor isi 5 parameter
    def __init__(self, id=None, nameParam=None, trainParam=None, jumAttrParam=None, jumKelasParam=None):
        if id is None:
            self._id = ObjectId()
        else:
            self._id = id
        self.name = nameParam
        self.train = trainParam
        self.jumAttr = jumAttrParam
        self.jumKelas = jumKelasParam

#RETURN method di bawah
#array atr 1 [1,2,3,0,0]
#array atr 2 [1,2,3,0,0]
#baris - jumlah atribut suatu objek. misal Annotation, atribute 2
#kolom - menunjukkan jumlah kelas value suatu atribut objek. misal atribute 1. posAnno -> dibagi menjadi 5 kelas (0-0.2, 0.2-0.4, dst)
def initArrayNol2d(baris, kolom):
    array = []
    array2d = []
    for i in range(baris):
        for i in range(kolom):
            array.append(0)
        array2d.append(array)
    print(array)
    return array
    pass


def getArrayOfCounterBayesAttr(objBayesObject):
    counterUser = 0
    insBayesObject = BayesObject(None, objBayesObject.name, objBayesObject.train, objBayesObject.jumAttr,
                                 objBayesObject.jumKelas)
    jumlahAttr = insBayesObject.jumAttr
    jumlahKelas = insBayesObject.jumKelas
    name = insBayesObject.name
    for trainUser in insBayesObject.train:
        # for trainUser in trainNormal:

        counterUser += 1
        print("user : {}".format(counterUser))
        print(trainUser['username'])

        userTweetTrain = UserTweet(trainUser['_id'], trainUser['username'], trainUser['tweets'])
        # 2. for user ambil array tweetnya
        tweetarray = userTweetTrain.tweets
        # print(tweetarrayNormal)
        countrow = 0
        countPosAno = 0
        countNegAno = 0

        for tweet in tweetarray:
            countrow += 1
            # print("row : {}".format(countrow))
            tweetRow = TweetRow.build_from_json(tweet)

            if (name == "annotation"):
                annotation = Annotation.build_from_json(tweetRow.annotation)
            # annotation = Annotation.build_from_json(tweetRow.annotation) #diganti if diatasnya
            # print(annotation.__dict__)
            # print(annotation.posAnno)
            # 3. annotation 1, pos tag 2, sentence sentimen 3
            lenAttr = initArrayNol(jumlahAttr)
            countAttr = initArrayNol(jumlahAttr)
            # lenPos = len(annotation.posAnno)   # digantikan di dalam for di bawah nya
            # lenNeg = len(annotation.negAnno)
            # 4. kalkulasi anotation

            for attr in range(jumlahAttr):
                if (name == "annotation"):
                    if attr == 0:
                        lenAttr[attr] = len(annotation.posAnno)
                    elif attr == 1:
                        lenAttr[attr] = len(annotation.negAnno)
                countAttr[attr] += lenAttr[attr]

        temCounterAllAttr = 0
        for attr in range(jumlahAttr):
            temCounterAllAttr += countAttr[attr]
            print(" Jumlah dari attribut {} user ini : {} ".format(attr, countAttr[attr]))
        sumCountAttr = temCounterAllAttr
        print(" Total jumlah attribut user ini : {} ".format(sumCountAttr))
        # countPosAno += lenPos   #digantikan for diatas
        # countNegAno += lenNeg

        # sumAno = countPosAno + countNegAno                          #digantikan for di atas
        # print(" Jumlah positif user ini : {} ".format(countPosAno))
        # print(" Jumlah negatif user ini : {} ".format(countNegAno))
        # print(" Total annotation user ini : {} ".format(sumAno))

        # normalisasi data positif negatif dari kelas depresi dan normal
        valueAttrNormal = initArrayNol(jumlahAttrAno)

        # valuePosAnoNormal = normalize(countPosAno, sumAno)  #diganti var di atas, proses assign di bawah
        # valueNegAnoNormal = normalize(countNegAno, sumAno)
        # kelasPosAno = 0
        # kelasNegAno = 0
        kelasAno = initArrayNol(jumlahAttrAno)  # akan diisi user ini masuk kelas berapa di bagian pos, neg
        for i in range(jumlahAttrAno):
            valueAttrNormal[i] = normalize(countAttr[attr], sumCountAttr)
            kelasAno[i] = cariKelas(valueAttrNormal[i], jumlahKelas)  # sudah ketemu kelas apa di pos, neg

        # kelasPosAno = cariKelas(valuePosAnoNormal, jumlahKelasAnno) #digantikan for di atas
        # kelasNegAno = cariKelas(valueNegAnoNormal, jumlahKelasAnno)

        # counterBayesPosAno[kelasPosAno] += 1
        # counterBayesNegANo[kelasNegAno] += 1
        # arrayCounterBayes = []  # berisi jumAttr*jumKelas array
        arrayCounterBayes = initArrayNol2d(jumlahAttr, jumlahKelas)
        for attr in range(jumlahAttrAno):  # 2 attribut
            # 5 kelas, dia masuk yang mana kelasAno[attr] berisi kelas mana yang akan dimasuki nilai
            arrayCounterBayes[attr][kelasAno[attr]] += 1
            print("hasil bayes {} : {}".format(attr, arrayCounterBayes[attr][kelasAno[attr]]))

        # print("hasil bayes pos : {}".format(arrayCounterBayes)) #digantikan for di atas
        # print("hasil bayes neg : {}".format(counterBayesNegANo))
        print("--------------------------------------------------------------")
        # if counterUser ==30 :
        #     break
    return arrayCounterBayes


def getHasil(kelasPosAno, kelasNegAno):
    pass


if __name__ == '__main__':
    jumlahKelasAnno = 5
    jumlahAttrAno = 2
    counterBayesPosAno = initArrayNol(jumlahKelasAnno)  # akan diisi sebanyak pembagian kelas bayes
    counterBayesNegANo = initArrayNol(jumlahKelasAnno)
    counterBayesPosAnoDep = initArrayNol(jumlahKelasAnno)  # akan diisi sebanyak pembagian kelas bayes
    counterBayesNegANoDep = initArrayNol(jumlahKelasAnno)
    print(len(counterBayesPosAno))

    userNormalRepo = UserTweetNormRepo()
    trainNormal = userNormalRepo.read()
    #param
    # trainNormal-> hasil read DB
    # 2 jumlah atribut(kolom) suatu object yang akan dibayeskan.
    # 5-> jumlah pembagian kelas data kuantitatif di masing2 atribut(kolom)
    objBayesNormalAnnotation = BayesObject(None, "annotation",trainNormal,jumlahAttrAno, jumlahKelasAnno)
    # arrayOfCounterBayesAttrAnnottaion = getArrayOfCounterBayesAttr(objBayesNormalAnnotation)
    # print(arrayOfCounterBayesAttrAnnottaion)
    counterUser = 0

    # 1. read depresi user total 26, read normal user total 26
    for trainUser in trainNormal:
        counterUser += 1
        print("user : {}".format(counterUser))
        print(trainUser['username'])

        userTweetTrain = UserTweet(trainUser['_id'], trainUser['username'], trainUser['tweets'])
        # 2. for user ambil array tweetnya
        tweetarray = userTweetTrain.tweets
        # print(tweetarrayNormal)
        countrow = 0
        countPosAno = 0
        countNegAno = 0
        for tweet in tweetarray:
            countrow += 1
            # print("row : {}".format(countrow))

            tweetRow = TweetRow.build_from_json(tweet)
            annotation = Annotation.build_from_json(tweetRow.annotation)
            # print(annotation.__dict__)
            # print(annotation.posAnno)
            # 3. annotation 1, pos tag 2, sentence sentimen 3
            lenPos = len(annotation.posAnno)
            lenNeg = len(annotation.negAnno)
            # 4. kalkulasi anotation
            countPosAno += lenPos
            countNegAno += lenNeg
        sumAno = countPosAno + countNegAno
        print(" Jumlah positif user dep ini : {} ".format(countPosAno))
        print(" Jumlah negatif user dep ini : {} ".format(countNegAno))
        print(" Total annotation user ini : {} ".format(sumAno))
        # normalisasi data positif negatif dari kelas depresi dan normal
        valuePosAnoNormal = normalize(countPosAno, sumAno)
        valueNegAnoNormal = normalize(countNegAno, sumAno)
        kelasPosAno = 0
        kelasNegAno = 0
        kelasPosAno = cariKelas(valuePosAnoNormal, jumlahKelasAnno)
        kelasNegAno = cariKelas(valueNegAnoNormal, jumlahKelasAnno)

        # if kelasPosAno == 4:
            # print("ME")
        counterBayesPosAno[kelasPosAno] += 1
        counterBayesNegANo[kelasNegAno] += 1
        print("hasil bayes pos : {}".format(counterBayesPosAno))
        print("hasil bayes neg : {}".format(counterBayesNegANo))
        print("--------------------------------------------------------------")

        # if counterUser ==30 :
        #     break

    # temObjUserTweet =

    print("====================================================")


    # temObjUserTweet =

    print("====================================================")
    print(" JUMLAH USER NORMAL yang ditrain : {} ".format(counterUser))

    userDepressionRepo = UserTweetRepository()
    trainDepression = userDepressionRepo.read()

    counterUser = 0
    # 1. read depresi user total 26, read normal user total 26
    for trainUser in trainDepression:
        counterUser += 1
        print("user : {}".format(counterUser))
        print(trainUser['username'])

        userTweetTrain = UserTweet(trainUser['_id'], trainUser['username'], trainUser['tweets'])
        # 2. for user ambil array tweetnya
        tweetarray = userTweetTrain.tweets
        # print(tweetarrayNormal)
        countrow = 0
        countPosAno = 0
        countNegAno = 0
        for tweet in tweetarray:
            countrow += 1
            # print("row : {}".format(countrow))

            tweetRow = TweetRow.build_from_json(tweet)
            annotation = Annotation.build_from_json(tweetRow.annotation)
            # print(annotation.__dict__)
            # print(annotation.posAnno)
            # 3. annotation 1, pos tag 2, sentence sentimen 3
            lenPos = len(annotation.posAnno)
            lenNeg = len(annotation.negAnno)
            # 4. kalkulasi anotation
            countPosAno += lenPos
            countNegAno += lenNeg
        sumAno = countPosAno + countNegAno
        print(" Jumlah positif user dep ini : {} ".format(countPosAno))
        print(" Jumlah negatif user dep ini : {} ".format(countNegAno))
        print(" Total annotation user ini : {} ".format(sumAno))
        # normalisasi data positif negatif dari kelas depresi dan normal
        valuePosAnoNormal = normalize(countPosAno, sumAno)
        valueNegAnoNormal = normalize(countNegAno, sumAno)
        kelasPosAno = 0
        kelasNegAno = 0
        kelasPosAno = cariKelas(valuePosAnoNormal, jumlahKelasAnno)
        kelasNegAno = cariKelas(valueNegAnoNormal, jumlahKelasAnno)

        # if kelasPosAno == 4:
        #     print("ME")
        counterBayesPosAnoDep[kelasPosAno] += 1
        counterBayesNegANoDep[kelasNegAno] += 1
        print("hasil bayes pos : {}".format(counterBayesPosAnoDep))
        print("hasil bayes neg : {}".format(counterBayesNegANoDep))
        print("--------------------------------------------------------------")

        # if counterUser ==30 :
        #     break

    # temObjUserTweet =

    print("====================================================")
    print(" JUMLAH USER DEPRESI yang ditrain : {} ".format(counterUser))
    jumlahTrain = 35
    bayesProbPos = initArrayNol(jumlahKelasAnno)
    bayesProbNeg = initArrayNol(jumlahKelasAnno)
    bayesProbPosDep = initArrayNol(jumlahKelasAnno)
    bayesProbNegDep = initArrayNol(jumlahKelasAnno)
    for i in range(jumlahKelasAnno):
        bayesProbPos[i] = counterBayesPosAno[i]/jumlahTrain
        bayesProbNeg[i] = counterBayesNegANo[i]/jumlahTrain
        bayesProbPosDep[i] = counterBayesNegANoDep[i]/jumlahTrain
        bayesProbNegDep[i] = counterBayesPosAnoDep[i]/jumlahTrain

    jum = 10
    result = initArrayNol(jum)
    for i in range(jum):
        posAnoUser = random.randint(0,200)
        negAnoUser = random.randint(0,200)
        sumAnoUser = posAnoUser + negAnoUser
        print(" Jumlah positif user dep ini : {} ".format(posAnoUser))
        print(" Jumlah negatif user dep ini : {} ".format(negAnoUser))
        print(" Total annotation user ini : {} ".format(sumAnoUser))
        # normalisasi data positif negatif dari kelas depresi dan normal
        valuePosAnoNormalUser = normalize(posAnoUser, sumAnoUser)
        valueNegAnoNormalUser = normalize(negAnoUser, sumAnoUser)
        kelasPosAno = 0
        kelasNegAno = 0
        kelasPosAno = cariKelas(valuePosAnoNormalUser, jumlahKelasAnno)
        kelasNegAno = cariKelas(valueNegAnoNormalUser, jumlahKelasAnno)

        resNormal = bayesProbPos[kelasPosAno]*bayesProbNeg[kelasNegAno]
        resDepression = bayesProbPosDep[kelasNegAno]*bayesProbNegDep[kelasNegAno]

        if resNormal > resDepression:
            print("NORMAL USER")
        elif resDepression > resNormal:
            print("DEPRESSION USER")
        else:
            print("UNKNOWN USER")
        print("=========================================================")
        #
        # bayesValuePos
        # result = initArrayNol(jumlahAttrAno)
        # for i in range(len(result)):
        #
        #     result[i] = bayesProbPos[kelasPosAno] * bayesProbNeg[kelasNegAno]
        #     result[i] = bayesProbPosDep[kelasPosAno] * bayesProbNegDep[kelasNegAno]
        # if result[0]>result[1]:
        #     strResult =


    #panggil di method bayesClassAno->return array of probability class atrubut
    #misal method di bawah untuk atribut anotationPositif
    # dipecah 3 kelas 0-0.33 0.33-0.66 0.66-1 [named class 1 2 3]
    # class 1 : probability depresinya berapa
    # class 2 : probabilty depresinya berapa
    # class 3 : probabilty depresinya berapa
    # class 1 : probability normalnya berapa
    # class 2 : probabilty normalnya berapa
    # class 3 : probabilty normalnya berapa
    #RETURN 6 value karena atributnya 2 dan masing2 dipecah 3 kelas
    # kelasAtribut = 5
    # for tweet in tweetarray:
    #     # 3. annotation 1, pos tag 2, sentence sentimen 3
    #     lenPos = len(tweet.annotation.posAnno)
    #     lenNeg = len(tweet.annotation.negAno)
    #     # 4. kalkulasi anotation
    #     countPosAno += lenPos
    #     countNegAno += lenNeg
    # sumAno = countPosAno + countNegAno
    #normalisasi data positif negatif dari kelas depresi dan normal
    # valuePosAnoNormal = normalize(countPosAno, sum)
    # valueNegAnoNormal = normalize(countNegAno, sum)
    # counterClassPosAno = [2, 3, 4, 2] #akan diisi sebanyak pembagian kelas bayes
    # counterClassNegANo = []
    #untuk method cari kelas
    # kelas = valuePosAnoNormal / ( 1 / kelasAtribut) #misal : value = 0.2. 0.2/(1/5)
    # counterClassPosAno[kelas] += 1
    # print(" Jumlah positif user ini : {} ".format(countPosAno))
    # print(" Jumlah negatif user ini : {} ".format(countNegAno))
    # print(" Total annotation class ini : {} ".format(sumAno))




