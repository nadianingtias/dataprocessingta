import random
import pprint

from DBRepository.UserTweet import UserTweetRepository
from DBRepository.UserTweetNormalRepository import UserTweetNormalRepository as UserTweetNormRepo
from Model.LangStyle import UserTweet
from Model.LangStyle import Annotation
from Model.LangStyle import TweetRow
from Model.LangStyle import Sentence
import math
from bson.objectid import ObjectId

#METHOD untuk mengembalikan array of zero
def initArrayNol(jumIndex):
    array = []
    for i in range(jumIndex):
        array.append(0)
    return array

#METHOD untuk menormalisasi 'angka' di dalam range 'jumlah'
def normalize(angka, jumlah):
    return angka/jumlah

if __name__ == '__main__':
    # A. proses train user NORMAL
    userNormalRepo = UserTweetNormRepo()
    trainNormal = userNormalRepo.read()
    # B. proses train user DEPRESSION
    userDepressionRepo = UserTweetRepository()
    trainDepression = userDepressionRepo.read()
    # 1. read normal user total 35, read normal user total 26
    counterUser = 0
    usernames = []
    userSentiments = []
    for trainUser in trainDepression:
        counterUser += 1
        if counterUser > 35:
            break
        print("========================= user : {}".format(counterUser))
        print(trainUser['username'])
        usernames.append(trainUser['username'])
        #build object
        userTweetTrain = UserTweet(trainUser['_id'], trainUser['username'], trainUser['tweets'])
        # 2. for user ambil array tweetnya
        tweetarray = userTweetTrain.tweets
        countrow = 0
        countPosAno = 0
        countNegAno = 0
        sentimentPos = 0
        sentimentNeg = 0
        sentimentNeu = 0
        fluktuasi = []
        for tweet in tweetarray:
            countrow += 1
            #==build object tweetrow
            tweetRow = TweetRow.build_from_json(tweet)
            #==build object annotation
            annotation = Annotation.build_from_json(tweetRow.annotation)
            #==build object sentence
            # pprint.pprint(tweetRow.sentence)
            sentence = Sentence.build_from_json(tweetRow.sentence)
            #=========CALCULATE MULTISENTIMENT
            # multi = sentence.multiSentiment
            # cMulti = initArrayNol(5) # [ cpos, cneg, cneu, cunk, csum ]
            # for sent in multi:
            #     if sent == 'positive':
            #         cMulti[0] +=1
            #         cMulti[4] +=1
            #     elif sent == 'negative':
            #         cMulti[1] +=1
            #         cMulti[4] +=1
            #     elif sent == 'neutral':
            #         cMulti[2] +=1
            #         cMulti[4] += 1
            #     else:
            #         cMulti[3]+=1
            #         cMulti[4] += 1
            # bagMulti = initArrayNol(4) # [bagpos, bagneg, bagneu, bagunk]
            # print("csum : {}".format(cMulti[4]))
            # if cMulti[4] !=0: #cek agar tida kerror division by zero
            #     for i in range(len(bagMulti)):
            #         bagMulti[i] = cMulti[i] / cMulti[4]
            #     pprint.pprint(bagMulti)
            # else:
            #     print("hayo")

            #========CALCULATE UNISENTIMENT
            if sentence.uniSentiment == 'positive':
                # print("positif")
                sentimentPos += 1
                # fluktuasi.append(1)
            elif sentence.uniSentiment == 'negative':
                # print("negatif")
                sentimentNeg += 1
                # fluktuasi.append(-1)
            elif sentence.uniSentiment == 'neutral':
                sentimentNeu +=1
                # # DENGAN MULTISENTIMENT
                # if bagMulti[2] > 0.5:
                #     print("netral")
                #     print(bagMulti[2])
                #     sentimentNeu += 1
                # elif bagMulti[1] > 0 :
                #     sentimentNeg +=1
                # elif bagMulti[0] > 0 :
                #     sentimentPos += 1
                fluktuasi.append(0)
            else:
                print("we all dont know")


            # print("sentimene pos:{} neg:{} neu:{}".format(sentimentPos, sentimentNeg, sentimentNeu))

        print("row tweet user ini : {}".format(countrow))
        userSentiment = [sentimentPos, sentimentNeg, sentimentNeu]
        userSentiments.append(userSentiment)
        pprint.pprint(userSentiment)

        # sumsenti = sentimentPos + sentimentNeg + sentimentNeu
        sumsenti = sentimentPos + sentimentNeg

        if sumsenti !=0:
            print(sumsenti)
            for i in range(len(userSentiment)-1):
                userSentiment[i] = normalize(userSentiment[i], sumsenti)
            pprint.pprint("user sent : {}".format(userSentiment))

        #karena percobaan sebelumnya atribut netral dapat diabaikan,
        # maka yang diambil hanya atribut pos dan neg
        # penentuan kelas diambil bukan hanya dari range dibagi banyak kelas,
        # kelas dibagi berdasarkan selisih dari
